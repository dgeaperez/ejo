<?php

class EjoModule extends CWebModule {
  /**
   * EJO
   * ---
   * 
   * Hierarchy and Order Editor
   * ==========================
   * 
   * Instalation:
   * ------------
   * - Download from Yii's Extensions Section or from the [repository](https://bitbucket.org/dgeaperez/ejo)
   * - Unzip the content (or pull from) into protected/modules/ejo
   * - Edit main.php:
   *   Only one instance case:
   *    'modules'=>array(
   *      'ejo',
   *      ...
   * 
   *   Only one instance case (customized):
   *    'modules'=>array(
   *      'AnyName' => array(
   *         'class' => 'application.modules.ejo.EjoModule', //mandatory
   *      //in this case the tables and views will be named with
   *      //'AnyName' as seed
   *      )
   *      ...
   * 
   *   More than one instance case (customized):
   *    'modules'=>array(
   *      'AnyName' => array(
   *         'class' => 'application.modules.ejo.EjoModule', //mandatory
   *      //in this case the tables and views will be named with
   *      //'AnyName' as seed
   *      )
   *      'AnyNameOther' => array(
   *         'class' => 'application.modules.ejo.EjoModule', //mandatory
   *         // If the following parameters are not set, the ID will be used
   *         'mainTable = 'MyTable'; //Don't use prefix. Default: ID
   *         'relationsTable = 'MyRelationsTable'; //Don't use prefix. Default IDRelations
   *         'mainView = 'MyTableView'; //A name for the SQL View. Default: IDView
   *         'relationsView = 'MyRelationsTableView'; //A name for the SQL View. Default: IDRelationsView
   *         'prefixTable = 'tbl_'; //If not set, uses the Yii Deafult
   *         'prefixView = 'viw_'; //Yii hasn't any reference, so I suggest this
   *         'gemiInstalled = false; //You can avoid the use of GeMI module if it is installed
   *      )
   *      ...
   * 
   * NOTES:
   * ------
   * Perhaps the tables and views names are long or weird.
   * This module was designed to offer all the data needed without put the hands
   * on the tables...
   * Too, I think if you use this module, offer the benefit to do the dirty work
   * about tables, fields, etc.
   * When you recover your eyes after see the "tree editor", I can argue in favor of me
   * that I used code I sure works. And using JSTree or similar will slowdown my implementation
   * so feel free to fork it and share it :D (if I didn't yet)
   * 
   * Use:
   * ----
   * - Instantiate the module:
   * 
   *
   * 
   * Sets the entity's name to hierarchy and rank.
   * If you need work with several entities, set the diferent ID name in main.php 
   * for each instance.
   * If name is the ID seted.
   * @property string name 
   * 
   * Sets the entity's table name to hierarchy and rank.
   * Based on entity's name.
   * @property string mainTable 
   * 
   * Sets the entity relationships' table name to hierarchy and rank.
   * Based on entity's name.
   * @property string relationsTable
   * 
   * Sets the entity languages' view name to hierarchy and rank.
   * This view helps to i18n when GeMI module is installed
   * This is a SQL View to easy some queries. Its prefix is "vew_"
   * Based on entity's name.
   * @property string mainView 
   * 
   * Sets the entity relationships' view name to hierarchy and rank.
   * This is a SQL View to easy some queries. Its prefix is "vew_"
   * Based on entity's name.
   * @property string relationsView 
   * 
   * Sets the table prefix.
   * This will be readed from main.php, set in the module configuration
   * or when themodule is used.
   * @property string prefixTable 
   *
   * Sets the views prefix.
   * This will be set in the module configuration or when themodule is used.
   * @property string prefixTable 
   *
   * Returns true if GeMI is installed, else false.
   * @property string gemiInstalled 
   *
   * Sets the singular title for CRUD views.
   * @property string titleSingular 
   *
   * Sets the plural title for CRUD views.
   * @property string titlePlural 
   * 
   * Sets the index's title for CRUD views.
   * This string will be evaluated, so you can set like:
   * 'titleIndex' => '$this->titlePlural . " Index"';
   * @property string titleIndex 
   * 
   * Sets the create's title for CRUD views.
   * This string will be evaluated, so you can set like:
   * 'titleCreate' => '$this->titleSingular . " Create"';
   * @property string titleCreate 
   * 
   * Sets the update's title for CRUD views.
   * This string will be evaluated, so you can set like:
   * 'titleUpdate' => '$this->titleSingular . " Update"';
   * @property string titleUpdate 
   * 
   * Sets the delete's title for CRUD views.
   * This string will be evaluated, so you can set like:
   * 'titleDelete' => '$this->titleSingular . " Delete"';
   * @property string titleDelete 
   * 
   * Sets the title for CRUD hierarchy and order editor's view.
   * This string will be evaluated, so you can set like:
   * 'titleHierEdit' => '$this->titlePlural . " Hierarchy and Order Editor"';
   * @property string titleHierEdit 
   * 
   * Sets the route to the item's target controller.
   * This value is used when MenuItems() returns the array to pass to CMenu, EMenu, etc.
   * @property string route 
   * 
   * Sets the code of messages for GeMI.
   * @property string codeMsg 
   * 
   * Shows all items, actived or not.
   * Values: true, false or a string to be evaluated
   * @property string showAll 
   */
  //ITEM MAIN TABLE STRUCTURE

  const ITEM_MAIN_TABLE_STRUCTURE = <<<FIN
CREATE TABLE `TABLE_NAME` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `itmCodigo` int(11) NOT NULL,
 `itmActivo` tinyint(1) DEFAULT '0',
 `idiCodigo` varchar(45) NOT NULL,
 `itmDescripcion` varchar(45) NOT NULL,
 `itmOrden` int(11) DEFAULT '0',
 `itmBaja` datetime DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `idiCodigo` (`idiCodigo`),
 KEY `codigo` (`itmCodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
FIN;
  const ITEM_MAIN_TABLE_DATA = <<<FIN
INSERT INTO `TABLE_NAME` 
  (`id`, `itmCodigo`, `itmActivo`, `idiCodigo`, `itmDescripcion`, `itmOrden`, `itmBaja`) 
VALUES
  (1, 0, 1, 'LANGUAGE', 'raiz', 0, NULL);
FIN;
  const ITEM_RELATIONS_TABLE_STRUCTURE = <<<FIN
CREATE TABLE `TABLE_NAME` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `itmAncestro` int(11) NOT NULL,
 `itmCodigo` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `itmCodigoAnc` (`itmAncestro`),
 KEY `itmCodigoDes` (`itmCodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
FIN;
  //TODO WHEN GEMI IS NOT INSTALLED tbl_idioma DOESN'T EXISTS!
  const ITEM_MAIN_VIEW_STRUCTURE_WITH_GEMI = <<<FIN
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `VIEW_NAME` AS 
  select if(isnull(`MAIN_TABLE`.`itmCodigo`),-(1),`MAIN_TABLE`.`itmCodigo`) AS `itmCodigo`,
  `tbl_idioma`.`idiCodigo` AS `idiCodigo`,
  `tbl_idioma`.`idiDescripcion` AS `idiDescripcion`,
  if(isnull(`MAIN_TABLE`.`itmDescripcion`),'',
  `MAIN_TABLE`.`itmDescripcion`) AS `itmDescripcion` 
  from (`tbl_idioma` 
     left join `MAIN_TABLE` 
     on((`MAIN_TABLE`.`idiCodigo` = `tbl_idioma`.`idiCodigo`))) 
  order by `tbl_idioma`.`idiDescripcion`
FIN;
  const ITEM_RELATIONS_VIEW_STRUCTURE_WITH_GEMI = <<<FIN
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `VIEW_NAME` AS 
  select `RELATION_TABLE`.`itmAncestro` AS `itmAncestro`,
  `MAIN_TABLE`.`itmOrden` AS `itmOrden`,
  `MAIN_TABLE`.`itmCodigo` AS `itmCodigo`,
  `MAIN_TABLE`.`itmDescripcion` AS `itmDescripcion`,
  `MAIN_TABLE`.`idiCodigo` AS `idiCodigo` 
  from (`MAIN_TABLE` 
    join `RELATION_TABLE` 
     on((`MAIN_TABLE`.`itmCodigo` = `RELATION_TABLE`.`itmCodigo`))) 
  where ((`MAIN_TABLE`.`idiCodigo` = 'SL') 
    and isnull(`MAIN_TABLE`.`itmBaja`)) 
  order by `RELATION_TABLE`.`itmAncestro`,`MAIN_TABLE`.`itmOrden`
FIN;
  const ITEM_RELATIONS_VIEW_STRUCTURE_WITHOUT_GEMI = <<<FIN
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `VIEW_NAME` AS 
  select `RELATION_TABLE`.`itmAncestro` AS `itmAncestro`,
  `MAIN_TABLE`.`itmOrden` AS `itmOrden`,
  `MAIN_TABLE`.`itmCodigo` AS `itmCodigo`,
  `MAIN_TABLE`.`itmDescripcion` AS `itmDescripcion`,
  `MAIN_TABLE`.`idiCodigo` AS `idiCodigo` 
  from (`MAIN_TABLE` 
    join `RELATION_TABLE` 
     on((`MAIN_TABLE`.`itmCodigo` = `RELATION_TABLE`.`itmCodigo`))) 
  where isnull(`MAIN_TABLE`.`itmBaja`) 
  order by `RELATION_TABLE`.`itmAncestro`,`MAIN_TABLE`.`itmOrden`
FIN;

  public $_mainTable = '';
  public $_relationsTable = '';
  public $_mainView = '';
  public $_relationsView = '';
  private $_name = '';
  private $_prefixTable = '';
  private $_prefixView = '';
  private $_gemiInstalled;
  private $_titleSingular = '';
  private $_titlePlural = '';
  private $_titleIndex = '';
  private $_titleCreate = '';
  private $_titleUpdate = '';
  private $_titleDelete = '';
  private $_titleView = '';
  private $_titleHierEdit = '';
  private $_route = '';
  private $_codeMsg = '';
  private $_showAll = true;
  private $_sl = '';
  private $_slDescripcion = '';
  private $_assetsUrl;
  private $_afterCreate = '';
  private $_afterDelete = '';
  private $_afterUpdate = '';
  private $_afterConnect = '';

  public function init() {
    // this method is called when the module is being created
    // you may place code here to customize the module or the application
    // import the module-level models and components
    $this->setImport(array(
        'application.modules.ejo.models.*',
        'application.modules.ejo.controllers.*',
        'application.controllers.*',
    ));

    $this->_name = $this->id;

    //If some parameter is not set, then it 'll set with a default value:
    if (empty($this->prefixTable))
      $this->prefixTable = Yii::app()->db->tablePrefix;
    if (empty($this->prefixView))
      $this->prefixView = 'vew_';
    if (empty($this->mainTable))
      $this->mainTable = $this->name;
    if (empty($this->mainView))
      $this->mainView = $this->name . "View";
    if (empty($this->relationsTable))
      $this->relationsTable = $this->name . "Relations";
    if (empty($this->relationsView))
      $this->relationsView = $this->name . "RelationsView";
    if (empty($this->gemiInstalled)) {
      $this->_gemiInstalled = Yii::app()->hasModule('gemi');
    }
    if (empty($this->titleSingular))
      $this->titleSingular = "Item";
    if (empty($this->titlePlural))
      $this->titlePlural = "Items";
    if (empty($this->titleIndex))
      $this->titleIndex = '$this->titlePlural . " Index"';
    if (empty($this->titleCreate))
      $this->titleCreate = '$this->titleSingular . " Create"';
    if (empty($this->titleUpdate))
      $this->titleUpdate = '$this->titleSingular . " Update"';
    if (empty($this->titleDelete))
      $this->titleDelete = '$this->titleSingular . " Delete"';
    if (empty($this->titleView))
      $this->titleView = '$this->titleSingular . " View"';
    if (empty($this->titleHierEdit))
      $this->titleHierEdit = '$this->titlePlural . " Hierarchy and Order Editor"';
    if (empty($this->route))
      $this->route = '#';
    if (empty($this->codeMsg))
      $this->codeMsg = $this->name;
    if (empty($this->showAll))
      $this->showAll = true;
    if (empty($this->_sl))
      $this->_sl = Yii::app()->sourceLanguage;

    if ($this->_gemiInstalled) {
      $this->_relationsView = $this->_relationsView . '_GeMI';
      $idioma = Idioma::model()->findByAttributes(array('idiCodigo' => $this->_sl));
      $this->_slDescripcion = $idioma->idiDescripcion;
      unset($idioma);
    }

    if (class_exists('Item')) {
      Item::$_mainTable = $this->mainTable;
      Item::$_relationsTable = $this->relationsTable;
      Item::$_mainView = $this->mainView;
      Item::$_relationsView = $this->relationsView;
      Item::$_codeMsg = $this->codeMsg;
      Item::$_titleSingular = $this->titleSingular;
      Item::$_titlePlural = $this->titlePlural;
      Item::$_route = $this->route;
      Item::$_gemiInstalled = $this->gemiInstalled;
      Item::$_sl = Yii::app()->sourceLanguage;
      ItemRelacion::$_mainTable = $this->mainTable;
      ItemRelacion::$_relationsTable = $this->relationsTable;
      ItemRelacion::$_mainView = $this->mainView;
      ItemRelacion::$_relationsView = $this->relationsView;
      ItemRelacion::$_codeMsg = $this->codeMsg;
      ItemRelacion::$_titleSingular = $this->titleSingular;
      ItemRelacion::$_titlePlural = $this->titlePlural;
      ItemRelacion::$_route = $this->route;
      ItemRelacion::$_gemiInstalled = $this->gemiInstalled;
    }
    $this->checkTables();
  }

  /**
   * Setters & Getters
   */
  public function getName() {
    return $this->_name;
  }

  public function getAfterCreate() {
    return $this->_afterCreate;
  }

  public function setAfterCreate($afterCreate) {
    $this->_afterCreate = $afterCreate;
  }

  public function getAfterDelete() {
    return $this->_afterDelete;
  }

  public function setAfterDelete($afterDelete) {
    $this->_afterDelete = $afterDelete;
  }

  public function getAfterUpdate() {
    return $this->_afterUpdate;
  }

  public function setAfterUpdate($afterUpdate) {
    $this->_afterUpdate = $afterUpdate;
  }

  public function getAfterConnect() {
    return $this->_afterConnect;
  }

  public function setAfterConnect($afterConnect) {
    $this->_afterConnect = $afterConnect;
  }

  public function setPrefixTable($prefixTable = '') {
    $this->_prefixTable = $prefixTable;
  }

  public function getPrefixTable() {
    return $this->_prefixTable;
  }

  public function setPrefixView($prefixView = '') {
    $this->_prefixView = $prefixView;
  }

  public function getPrefixView() {
    return $this->_prefixView;
  }

  public function setMainTable($mainTable = '') {
    $this->_mainTable = $this->prefixTable . $mainTable;
  }

  public function getMainTable() {
    return $this->_mainTable;
  }

  public function setRelationsTable($relationsTable = '') {
    $this->_relationsTable = $this->prefixTable . $relationsTable;
  }

  public function getRelationsTable() {
    return $this->_relationsTable;
  }

  public function setMainView($mainView = '') {
    $this->_mainView = $this->prefixView . $mainView;
  }

  public function getMainView() {
    return $this->_mainView;
  }

  public function setRelationsView($relationsView = '') {
    $this->_relationsView = $this->prefixView . $relationsView;
  }

  public function getRelationsView() {
    return $this->_relationsView;
  }

  public function setGemiInstalled($gemiInstalled) {
    $this->_gemiInstalled = $gemiInstalled;
  }

  public function getGemiInstalled() {
    return $this->_gemiInstalled;
  }

  public function setTitleSingular($titleSingular) {
    $this->_titleSingular = $titleSingular;
  }

  public function getTitleSingular() {
    return $this->_titleSingular;
  }

  public function setTitlePlural($titlePlural) {
    $this->_titlePlural = $titlePlural;
  }

  public function getTitlePlural() {
    return $this->_titlePlural;
  }

  public function setTitleIndex($titleIndex) {
    $this->_titleIndex = eval("return $titleIndex;");
  }

  public function getTitleIndex() {
    return $this->_titleIndex;
  }

  public function setTitleCreate($titleCreate) {
    $this->_titleCreate = eval("return $titleCreate;");
  }

  public function getTitleCreate() {
    return $this->_titleCreate;
  }

  public function setTitleUpdate($titleUpdate) {
    $this->_titleUpdate = eval("return $titleUpdate;");
  }

  public function getTitleUpdate() {
    return $this->_titleUpdate;
  }

  public function setTitleDelete($titleDelete) {
    $this->_titleDelete = eval("return $titleDelete;");
  }

  public function getTitleDelete() {
    return $this->_titleDelete;
  }

  public function setTitleView($titleView) {
    $this->_titleView = eval("return $titleView;");
  }

  public function getTitleView() {
    return $this->_titleView;
  }

  public function setTitleHierEdit($titleHierEdit) {
    $this->_titleHierEdit = eval("return $titleHierEdit;");
  }

  public function getTitleHierEdit() {
    return $this->_titleHierEdit;
  }

  public function setRoute($route) {
    $this->_route = $route;
  }

  public function getRoute() {
    return $this->_route;
  }

  public function setCodeMsg($codeMsg) {
    $this->_codeMsg = $codeMsg;
  }

  public function getCodeMsg() {
    return $this->_codeMsg;
  }

  public function setShowAll($showAll) {
    $this->_showAll = $showAll;
  }

  public function getShowAll() {
    return $this->_showAll;
  }

  public function getSl() {
    return $this->_sl;
  }

  public function getSlDescripcion() {
    return $this->_slDescripcion;
  }

  /**
   * End Setters & Getters 
   */

  /**
   * Lists the items created.
   * If GeMI module is installed, setting $lng will return the list in that language
   * or the default application language
   * @param type string $lng Language code to obtain the list of items
   */
  public function ListItems($lng = null) {
    if ($lng === null)
      $lng = $this->_sl;
    return ItemController::ListarItems($lng);
  }

  /**
   * Lists the paths created.
   * Path: according to the made hierarchy you'll get a path. It allways begin
   * with '/' and the items will be separated with '/'.
   * If GeMI module is installed, setting $lng will return the list in that language
   * or the default application language
   * @param type string $lng Language code to obtain the list of items
   * @return array
   */
  public function ListPaths($iSemilla = 0, $sRuta = '0', $sCamino = "/", $lng = null) {
    return ItemController::ListarRutasPosibles($iSemilla, $sRuta, $sCamino, $lng);
  }

  /**
   * Lists the paths created with the items' ID.
   * Path: according to the made hierarchy you'll get a path. It allways begin
   * with '/' and the items will be separated with '/'.
   * @return array
   */
  public function ListPathsItemsID() {
    return ItemController::ListarRelaciones();
  }

  /**
   * Lists item cointaining paths.
   * If 'item' is not specified, will return the same result as ListPaths
   * Path: according to the made hierarchy you'll get a path. It allways begin
   * with '/' and the items will be separated with '/'.
   * If GeMI module is installed, setting $lng will return the list in that language
   * or the default application language
   * @param type string $item Item to search
   * @param type string $lng Language code to obtain the list of items
   * @return array
   */
  public function ListPathsWithItem($item = '', $lng = '') {
    return Item::model()->ListPathsWithItem($item, $lng);
  }

  /**
   * Lists item cointaining paths. The paths are formed by item ID
   * If 'item' is not specified, will return the same result as ListPathsItemsId
   * Path: according to the made hierarchy you'll get a path. It allways begin
   * with '/' and the items will be separated with '/'.
   * @param type string $item Item to search
   * @param type string $lng Language code to obtain the list of items
   * @return array
   */
  public function ListPathsIDsWithItem($item = '') {
    return Item::model()->ListPathsIDsWithItem($item, $lng);
  }

  /**
   * Returns an array to use with CMenu.
   * I preffer EMenu, because it lets use several levels.
   */
  public function getMenuArray() {
    return ItemController::getMenuArray();
  }

  /**
   * Returns an array to use with CMenu.
   * I preffer EMenu, because it lets use several levels.
   */
  public function TranslatePathID($path) {
    return ItemController::TraducirRuta($path);
  }

  public function checkTables() {
    $_database = Yii::app()->db
            ->createCommand('SELECT DATABASE()')
            ->queryScalar();

    //Checking Main Table
    $tbl_existe = Yii::app()->db
                    ->createCommand(
                            "SELECT COUNT(*) AS Existe " .
                            "FROM information_schema.tables " .
                            "WHERE table_schema = '$_database'" .
                            "AND table_name = '" . $this->mainTable . "'")
                    ->queryScalar() > 0;

    if (!$tbl_existe) {
      $sql = str_replace('TABLE_NAME', $this->maintable, self::ITEM_MAIN_TABLE_STRUCTURE);
      Yii::app()->db->createCommand($sql)->execute();

      $sql = str_replace(
              array('TABLE_NAME', 'LANGUAGE'), array($this->mainTable, Yii::app()->language), self::ITEM_MAIN_TABLE_DATA);
      Yii::app()->db->createCommand($sql)->execute();
    }

    //Checking Relations Table
    $tbl_existe = Yii::app()->db
                    ->createCommand(
                            "SELECT COUNT(*) AS Existe " .
                            "FROM information_schema.tables " .
                            "WHERE table_schema = '$_database' " .
                            "AND table_name = '" . $this->relationsTable . "'")
                    ->queryScalar() > 0;

    if (!$tbl_existe) {
      $sql = str_replace(
              'TABLE_NAME', $this->relationsTable, self::ITEM_RELATIONS_TABLE_STRUCTURE);
      Yii::app()->db->createCommand($sql)->execute();
    }

    //Checking Main View
    $vst_existe = Yii::app()->db
                    ->createCommand(
                            "SELECT COUNT(*) AS Existe " .
                            "FROM information_schema.tables " .
                            "WHERE table_schema = '$_database' " .
                            "AND table_name = '" . $this->mainView . "'")
                    ->queryScalar() > 0;

    if (!$vst_existe) {
      $sql = str_replace(
              array('VIEW_NAME', 'MAIN_TABLE'), array($this->mainView, $this->mainTable), self::ITEM_MAIN_VIEW_STRUCTURE_WITH_GEMI
      );
      Yii::app()->db->createCommand($sql)->execute();
    }

    //Checking Relations View
    $vst_existe = Yii::app()->db
                    ->createCommand(
                            "SELECT COUNT(*) AS Existe " .
                            "FROM information_schema.tables " .
                            "WHERE table_schema = '$_database' " .
                            "AND table_name = '" . $this->relationsView . "'")
                    ->queryScalar() > 0;

    if (!$vst_existe) {
      $sql = str_replace(
              array('VIEW_NAME', 'MAIN_TABLE', 'RELATION_TABLE', 'SL'), array($this->relationsView, $this->mainTable, $this->relationsTable, $this->sl), self::ITEM_RELATIONS_VIEW_STRUCTURE_WITH_GEMI
      );
      Yii::app()->db->createCommand($sql)->execute();
    }

    //Now, checking if a previous view exist and not be there dpending the
    //GeMI existance
    if ($this->_gemiInstalled) {
      if (substr($this->_relationsView, -5) === '_GeMI')
        $oldViewName = substr($this->relationsView, 0, strlen($this->relationsView) - 5);
    } else {
      if (substr($this->_relationsView, -5) !== '_GeMI')
        $oldViewName = $this->_relationsView . '_GeMI';
    }

    Yii::app()->db
            ->createCommand("DROP VIEW IF EXISTS $oldViewName")
            ->execute();

    /* Add here the subsequent modifications and the procedures to update 
     * the tables/views strutures
     */
  }
  
  public function getListPaths(){
    return ItemController::ListarRutas;
  }

  public function getAssetsUrl(){
        if ($this->_assetsUrl === null)
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('application.modules.ejo').DIRECTORY_SEPARATOR.'assets' );
        return $this->_assetsUrl;
  }
}