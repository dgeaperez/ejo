<?php
$title = $this->gemiInstalled ?
        $model[$this->sl]->titulo() :
        $model->titulo();

$this->breadcrumbs = array($title => array('index'),
    Yii::t('int_GRL_CREAR', $this->titleCreate),
);

$this->menu = array(
    array('label' => Yii::t('int_GRL_LISTAR', $this->titleIndex), 'url' => array('index')),
        //array('label'=>Yii::t('int_GRL_ADMINISTRAR', 'Administrar'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('int_GRL_CREAR', $this->titleCreate); ?></h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>