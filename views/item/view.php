<?php
$this->breadcrumbs = array(
    $model->titulo() => array('index'),
    $model->itmDescripcion,
);

$this->menu = array(
    array('label' => Yii::t('int_GRL_LISTAR', $this->titleIndex), 'url' => array('index')),
    array('label' => Yii::t('int_GRL_CREAR', $this->titleCreate), 'url' => array('create')),
    array('label' => Yii::t('int_GRL_MODIFICAR', $this->titleUpdate), 'url' => array('update', 'id' => $model->itmCodigo)),
    array('label' => Yii::t('int_GRL_ELIMINAR', $this->titleDelete), 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->itmCodigo), 'confirm' =>Yii::t('int_GRL_ELIMINAR_PREG', '¿Está seguro que desea eliminar este registro?'))),
    array('label'=>Yii::t('int_GRL_RELACIONAR', $this->titleHierEdit), 'url'=>array('connect'), 
          'visible' => $this->ItemsDefinidos()>1),
    //array('label' => Yii::t('int_GRL_ADMINISTRAR', 'Administrar'), 'url' => array('admin')),
);
?>

<h1><?php echo Yii::t('int_GRL_VER','Ver') . ' ' . $model->itmDescripcion; ?></h1>

<?php
$sSQL = <<<EOT
SELECT COUNT(*)
FROM tbl_seccion
RIGHT OUTER JOIN tbl_idioma
  ON (tbl_seccion.idiCodigo = tbl_idioma.idiCodigo) 
  AND (tbl_seccion.itmCodigo = $model->itmCodigo) 
WHERE (tbl_idioma.idiCodigo != '$this->sl' and tbl_idioma.idiBaja IS NULL)
ORDER BY tbl_idioma.idiDescripcion;
EOT;
$count = Yii::app()->db->createCommand($sSQL)->queryScalar();
$sSQL = <<<EOT
SELECT 
IF(ISNULL(tbl_seccion.itmCodigo),-1, tbl_seccion.itmCodigo) AS itmCodigo, 
tbl_idioma.idiCodigo, 
tbl_idioma.idiDescripcion, 
IF(ISNULL(tbl_seccion.itmDescripcion),'',tbl_seccion.itmDescripcion) AS itmDescripcion 
FROM tbl_seccion
RIGHT OUTER JOIN tbl_idioma
  ON (tbl_seccion.idiCodigo = tbl_idioma.idiCodigo) 
  AND (tbl_seccion.itmCodigo = $model->itmCodigo) 
WHERE (tbl_idioma.idiCodigo != '$this->sl' and tbl_idioma.idiBaja IS NULL) 
ORDER BY tbl_idioma.idiDescripcion
EOT;
$seccionesTraducidas = new CSqlDataProvider($sSQL, array(
            'id' => 'seccionesTraducidas',
            'keyField' => 'idiDescripcion',
            'totalItemCount' => $count,
            'sort' => array(
                'attributes' => array(
                    'idiDescripcion',
                ),
            ),
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $seccionesTraducidas,
    'columns' => array(
        array(
            'header' => Yii::t('atr_SECC_IDIDESCRIPCION', 'Idioma'),
            'name' => 'idiDescripcion'),
        array(
            'header' => Yii::t('int_GRL_TRADUCCION', 'Traducción'),
            'name' => 'itmDescripcion'),
    ),
        )
);
?>
<h3><?php
$cadena = Yii::t('atr_GRL_ACTIVADA', 'Activada');
$cadena .= ': ';
$cadena .= $model->itmActivo ? Yii::t('int_GRAL_SI', 'Si') : Yii::t('int_GRAL_NO', 'No');
echo $cadena;
?></h3>
