<?php
$this->breadcrumbs=array(
$model->titulo()=>array('index'),
	Yii::t('int_GRL_RELACIONAR', $this->titleHierEdit),
);

$this->menu=array(
	array('label'=>Yii::t('int_GRL_LISTAR', $this->titleIndex), 'url'=>array('index')),
	array('label'=>Yii::t('int_GRL_CREAR', $this->titleCreate), 'url'=>array('create')),
);
?>

<h1><?php echo Yii::t('int_GRL_RELACIONAR',$this->titleHierEdit);?></h1>

<?php echo $this->renderPartial('_formcnx', array('items'=>$items, 
                                                  'relaciones'=>$relaciones)); ?>