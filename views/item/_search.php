<div class="wide form">

  <?php
  $form = $this->beginWidget('CActiveForm', array(
      'action' => Yii::app()->createUrl($this->route),
      'method' => 'get',
          ));
  ?>
  <div class="row">
    <?php echo $form->label($model, 'itmDescripcion'); ?>
    <?php echo $form->textField($model, 'itmDescripcion', array('size' => 30, 'maxlength' => 30)); ?>
  </div>

  <?php
  if ($this->gemiInstalled) {
    echo '  <div class="row">';
    echo $form->label($model, 'idiDescripcion');
    echo $form->dropDownList($model, 'idiCodigo', Yii::app()->getModule('gemi')->ListarIdiomasVigentes);
    echo '  </div>';
  }
  ?>

  <div class="row buttons">
    <?php echo CHtml::submitButton(Yii::t('int_GRL_BUSCAR', 'Buscar')); ?>
    <?php echo CHtml::resetButton(Yii::t('int_GRL_LIMPIAR', 'Limpiar'), array('id' => 'form-reset-button')); ?>
  </div>

  <?php $this->endWidget(); ?>

</div><!-- search-form -->