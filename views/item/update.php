<?php
if ($this->gemiInstalled) {
  $viewMenu = array('label' => Yii::t('int_GRL_VER', $this->titleView), 'url' => array('view', 'id' => $model[$this->sl]->itmCodigo));
  $part1 = $model[$this->sl]->titulo();
  $part2 = array($model[$this->sl]->itmDescripcion => array('view', 'id' => $model[$this->sl]->itmCodigo));
  $title = $model[$this->sl]->itmDescripcion;
} else {
  $viewMenu = array();
  $part1 = $model->titulo();
  $part2 = $model->itmDescripcion;
  $title = $model->itmDescripcion;
}

$this->breadcrumbs = array(
    $part1 => array('index'),
    $part2,
    Yii::t('int_GRL_MODIFICAR', $this->titleUpdate),
);

$this->menu = array(
    array('label' => Yii::t('int_GRL_LISTAR', $this->titleIndex), 'url' => array('index')),
    array('label' => Yii::t('int_GRL_CREAR', $this->titleCreate), 'url' => array('create')),
    $viewMenu,
    array('label' => Yii::t('int_GRL_RELACIONAR', $this->titleHierEdit), 'url' => array('connect'), 
          'visible' => $this->ItemsDefinidos()>1),
        //array('label'=>Yii::t('int_GRL_ADMINISTRAR', 'Administrar'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('int_GRL_MODIFICAR', $this->titleUpdate) . ' ' . $title ?></h1>

  <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
