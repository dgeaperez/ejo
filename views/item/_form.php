<div class="form">
  <?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'Seccion-form',
      'enableAjaxValidation' => false,
          ));
  if ($this->gemiInstalled) {
    $esUnAlta = $model[$this->sl]->isNewRecord;

    echo $form->errorSummary($model);
    foreach ($model as $i => $item) {
      ?>

      <div class="row">
        <?php
        echo $form->labelEx($item, "{$i}_idiDescripcion");
        echo $form->textField($item, "[$i]itmDescripcion");
        echo $form->error($item, "[$i]itmDescripcion");
        echo $form->hiddenField($item, "[$i]itmCodigo");
        echo $form->hiddenField($item, "[$i]idiCodigo");
        ?>
      </div>
      <?php
    }
    ?>
    <div class="row">
      <?php
      echo $form->labelEx($model[$this->sl], "itmActivo");
      echo $form->checkBox($model[$this->sl], "itmActivo");
      echo $form->error($model[$this->sl], "itmActivo");
    } else {
      $esUnAlta = $model->isNewRecord;

      echo $form->errorSummary($model);
      ?>

      <div class="row">
        <?php
        echo $form->labelEx($model, "itmDescripcion");
        echo $form->textField($model, "itmDescripcion");
        echo $form->error($model, "itmDescripcion");
        ?>
      </div>
      <div class="row">
        <?php
        echo $form->labelEx($model, "itmActivo");
        echo $form->checkBox($model, "itmActivo");
        echo $form->error($model, "itmActivo");
      }
      ?>
    </div>
    <div class="row buttons">
      <?php
      echo CHtml::submitButton($esUnAlta ? Yii::t('int_GRL_CREAR', 'Crear') : Yii::t('int_GRL_GUARDAR', 'Guardar'), array('id' => 'enviar'));
      ?>
    </div>

    <?php $this->endWidget(); ?>

  </div><!-- form -->