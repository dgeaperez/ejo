<?php
$this->breadcrumbs = array(
    $this->titleIndex,
);

$this->menu = array(
    array('label' => Yii::t('int_GRL_CREAR', $this->titleCreate), 'url' => array('create')),
    array('label' => Yii::t('int_GRL_RELACIONAR', $this->titleHierEdit), 'url' => array('connect'), 
          'visible' => $this->ItemsDefinidos()>1),
        //array('label'=>Yii::t('int_GRL_ADMINISTRAR', 'Administrar'), 'url'=>array('admin')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('seccion-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo $model->titulo(); ?> </h1>
<p><?php
echo Yii::t('inf_AYD_COMPARADOR', 'Opcionalmente puede utilizar un operador de comparación<br />' .
        'al comienzo de cada valor de la búsqueda para indicar cómo debe realizarse.<br />' .
        'Operadores: (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>, <b>=</b>)');
?></p>

<?php echo CHtml::link(Yii::t('int_GRL_BUSQ_AVANZADA', 'Búsqueda Avanzada'), '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
  <?php
  $this->renderPartial('_search', array(
      'model' => $model,
  ));
  ?>
</div><!-- search-form -->

<?php
$cmpCodigo = 'itmCodigo';
if ($this->gemiInstalled) {
  $tblNombre = $this->mainTable;
  $trnCampo = 'itmDescripcion';
  $sSQL = <<<EOT
SELECT COUNT(*)
FROM $tblNombre
WHERE ($tblNombre.idiCodigo = '$this->sl');
EOT;
  $count = Yii::app()->db->createCommand($sSQL)->queryScalar();

  $sSQL = "SELECT GROUP_CONCAT(tbl_idioma.idiDescripcion) as Lista from tbl_idioma WHERE tbl_idioma.idiCodigo <> '$this->sl' and tbl_idioma.idiBaja IS NULL;";
  $aReg = Yii::app()->db->createCommand($sSQL)->queryAll();
  $aColumnas = explode(',', 'Español,' . $aReg[0]['Lista']);
  $sColActivada = Yii::t('atr_' . $this->codeMsg . '_ITMACTIVO', 'Activado');
  $aCols = array('name' => $sColActivada,);
  foreach ($aColumnas as $c => $aColumna) {
    $aCols[] = array('name' => $aColumna);
  }
  $aCols[] = array('class' => 'CButtonColumn',
      'template' => '{view}{update}{delete}',
      'buttons' => array(
          'view' => array(
              'label' => Yii::t('int_GRL_VER', 'Ver'),
              'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data["' . $cmpCodigo . '"]))',
              'imageUrl' => Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets.gridview')) . "/view.png",
          ),
          'update' => array(
              'label' => Yii::t('int_GRL_MODIFICAR', 'Modificar'),
              'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data["' . $cmpCodigo . '"]))',
              'imageUrl' => Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets.gridview')) . "/update.png",
          ),
          'delete' => array(
              'label' => Yii::t('int_GRL_ELIMINAR', 'Eliminar'),
              'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data["' . $cmpCodigo . '"]))',
              'imageUrl' => Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets.gridview')) . "/delete.png",
          ),
      ),
  );

  $sSQL = <<<EOT
SELECT GROUP_CONCAT(
 "(SELECT t2.$trnCampo ",
 "FROM $tblNombre t2 ", 
 "WHERE t2.idiCodigo = '", tbl_idioma.idiCodigo, "'",
 " AND t1.$cmpCodigo=t2.$cmpCodigo) AS '", tbl_idioma.idiDescripcion,"'" 
SEPARATOR ', ') as PRE_SQL
from tbl_idioma
where tbl_idioma.idiCodigo <> '$this->sl' AND tbl_idioma.idiBaja IS NULL ;
EOT;

  $aReg = Yii::app()->db->createCommand($sSQL)->queryAll();
  $sPRE_SQL = empty($aReg[0]['PRE_SQL']) ? '' : ', '.$aReg[0]['PRE_SQL'];
  $sSI = Yii::t('int_GRAL_SI', 'Si');
  $sNO = Yii::t('int_GRAL_NO', 'No');
  $sSQL = <<<EOT
SELECT DISTINCT t1.$cmpCodigo,
IF(t1.itmActivo=0, "$sNO", "$sSI" ) as $sColActivada,
(select t2.$trnCampo 
 from $tblNombre t2 
 where t2.idiCodigo = '$this->sl'
 and t1.$cmpCodigo=t2.$cmpCodigo) as '$this->slDescripcion'
 $sPRE_SQL
 FROM $tblNombre t1
 WHERE t1.$cmpCodigo > 0
   ORDER BY $this->slDescripcion;
EOT;
  $grilla = Yii::app()->db->createCommand($sSQL)->queryAll();
  $agrilla = new CArrayDataProvider($grilla, array(
              'id' => 'grilla',
              'keyField' => $cmpCodigo,
              'totalItemCount' => $count,
              'pagination' => array(
                  'pageSize' => 10,
              ),
          ));
  $this->widget('zii.widgets.grid.CGridView', array(
      'dataProvider' => $agrilla,
      'columns' => $aCols,
          )
  );
} else {
  $this->widget('zii.widgets.grid.CGridView', array(
      'dataProvider' => $model->search(),
      'columns' => array(
          'itmDescripcion',
          array(
              'name'=>'itmActivo',
              'value'=>'$data->itmActivo ? Yii::t("int_GRL_SI","SI"):Yii::t("int_GRL_NO","NO")'
              ),
          array(
              'class' => 'CButtonColumn',
              'template' => '{update}{delete}',
              'buttons' => array(
                  'update' => array(
                      'label' => Yii::t('int_GRL_MODIFICAR', 'Modificar'),
                      'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data["' . $cmpCodigo . '"]))',
                      'imageUrl' => Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets.gridview')) . "/update.png",
                  ),
                  'delete' => array(
                      'label' => Yii::t('int_GRL_ELIMINAR', 'Eliminar'),
                      'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data["' . $cmpCodigo . '"]))',
                      'imageUrl' => Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets.gridview')) . "/delete.png",
                  ),
              ),
          ),
      ),
    )
  );
}
?>