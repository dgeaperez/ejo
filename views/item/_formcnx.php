<?php
$libjs=$this->module->assetsUrl.'/ejo.js';
echo "<script src=\"$libjs\" type=\"text/javascript\"></script>";
//preparo los valores de los arrays de items y relaciones
$jsDeItems = '';
$jsDeRelaciones = '';

foreach($items as $g=>$item){
  $aItems[] = $item->itmDescripcion;
  $jsDeItems .= "   aItems.push(new Array({$item->itmCodigo},'{$item->itmDescripcion}', {$item->itmOrden}));\r";  
}
foreach($relaciones as $r=>$relacion){
  $jsDeRelaciones .= "   aRelaciones.push(new Array({$relacion->itmAncestro},{$relacion->itmCodigo}, {$relacion->itmOrden}));\r";
}
?>
<div class="form">
<?php
  echo CHtml::beginForm();
?>
  <div class="row">
    <div id='SelectorItems' style='width: 50%; float: left;'>
    <?php
      echo CHtml::label(Yii::t('int_GRL_ALFABETICO','Alfabético'),'alf', array(
        'style'=>'display: inline;'));
      echo CHtml::radioButton('alf',true, array(
        'id'=>'alf', 
        'value'=>'alf',
        'onchange'=>'javascript:ActualizarLista("alf","items",document.getElementById("items").selectedIndex);'
        ));
      echo CHtml::label(Yii::t('int_GRL_JERARQUICO','Jerárquico'),'jer', array(
        'style'=>'display: inline;'));
      echo CHtml::radioButton('jer',true, array(
        'id'=>'jer', 
        'value'=>'jer',
        'onchange'=>'javascript:ActualizarLista("jer","items",document.getElementById("items").selectedIndex);'
        ));
      echo "<br />";
      echo CHtml::listBox('items',null,$aItems,array(
        'id'=>'items',
        'size'=>'22',
        'onchange'=>'javascript:ActualizarArbol(this.options[this.selectedIndex].value,"ArbolItems");'
      ));
    ?>
    </div>
    <div id='ArbolItems' style='margin: 0; padding: 0px; width: 50%; float: right;'>
     <p>elija item</p>
    </div>
  </div>
  <div class="clear"></div>
	<div class="row buttons">
		<?php 
      echo CHtml::button('|<', array(
        'id'=>'pri',
        'onclick'=>"javascript:CambiarPosicion('items','pri','ArbolItems');"
      ));
      echo CHtml::button('<', array(
        'id'=>'ant',
        'onclick'=>"javascript:CambiarPosicion('items','ant','ArbolItems');"
      ));
      echo CHtml::button('>', array(
        'id'=>'pos',
        'onclick'=>"javascript:CambiarPosicion('items','pos','ArbolItems');"
      ));
      echo CHtml::button('>|', array(
        'id'=>'ult',
        'onclick'=>"javascript:CambiarPosicion('items','ult','ArbolItems');"
      ));
      echo CHtml::submitButton(Yii::t('int_GRL_GUARDAR','Guardar'), array('id'=>'Guardar')); 
      echo CHtml::hiddenField('jerarquia','',array(
        'id'=>'jerarquia'));
      echo CHtml::hiddenField('relacion','',array(
        'id'=>'relacion'));
    ?>
	</div>
    <script type='text/javascript'>
    var aItems = new Array();
    var aRelaciones = new Array();
    var sDirImagenes = '<?php echo $this->module->assetsUrl;?>';
    <?php
        echo $jsDeItems;  
        echo $jsDeRelaciones;
        $sBoton = Yii::t('int_GRL_GUARDAR','Guardar');
    ?>
      ActualizarLista("alf","items");
      ActualizarArbol(document.getElementById("items").options[document.getElementById("items").selectedIndex].value,"ArbolItems");
      document.getElementById("Guardar").disabled = true;
    </script>

<?php echo CHtml::endForm(); 
?>

</div><!-- form -->