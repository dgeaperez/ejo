function ActualizarLista(sOrden, sObjeto, iPosicion){
  var oLista = document.getElementById(sObjeto).options;
  var i = 0;
  if (sOrden == "alf") {
    document.getElementById("alf").checked = true;
    document.getElementById("jer").checked = false;
    aItems = aItems.sort(OrdenarNombres);
  } else {
    document.getElementById("alf").checked = false;
    document.getElementById("jer").checked = true;
    aItems = aItems.sort(OrdenarJerarquias);
  }
  for (i=0; i < aItems.length; i++){
    if (oLista.length < 0){
      oLista[oLista.length] = new Option(aItems[i][1],aItems[i][0]);
    } else {
      oLista[i] = new Option(aItems[i][1],aItems[i][0]);
    }
  }
  document.getElementById(sObjeto).selectedIndex = iPosicion;
  document.getElementById("pri").disabled = sOrden == "alf" || iPosicion == 0;
  document.getElementById("ant").disabled = sOrden == "alf" || iPosicion == 0;
  document.getElementById("pos").disabled = sOrden == "alf" || iPosicion == document.getElementById(sObjeto).options.length-1;
  document.getElementById("ult").disabled = sOrden == "alf" || iPosicion == document.getElementById(sObjeto).options.length-1;
}

function OrdenarJerarquias(aItem1, aItem2){
  return aItem1[2] - aItem2[2];
}

function OrdenarNombres(aItem1, aItem2){
  if (aItem1[1] < aItem2[1]){
    return -1;
  } else if (aItem1[1] == aItem2[1]) {
    return 0;
  } else if (aItem1[1] > aItem2[1]) {
    return 1;
  }
}

function OrdenarNumeros(xItem1,xItem2){
  return xItem1-xItem2;
}

function ActualizarArbol(iSecCodigo, sIdentificacion) {
  var oArbolito;
  document.getElementById("pri").disabled = document.getElementById("alf").checked ||
                                            document.getElementById("items").selectedIndex == 0;
  document.getElementById("ant").disabled = document.getElementById("alf").checked ||
                                            document.getElementById("items").selectedIndex == 0;
  document.getElementById("pos").disabled = document.getElementById("alf").checked ||
                                            document.getElementById("items").selectedIndex == document.getElementById("items").options.length-1;
  document.getElementById("ult").disabled = document.getElementById("alf").checked ||
                                            document.getElementById("items").selectedIndex == document.getElementById("items").options.length-1;
  document.getElementById("jerarquia").value = JSON.stringify(aItems);
  document.getElementById("relacion").value = JSON.stringify(aRelaciones);
  
//  limpiar("ArbolItems","Descripcion");
//  document.getElementById("Descripcion").style.visibility = "hidden";
  oArbolito = document.getElementById(sIdentificacion);
  oArbolito.innerHTML=MostrarArbol(iSecCodigo);
//  document.getElementById("ArbolItems").innerHTML="<p>entre</p>";
//  document.getElementById("Descripcion").style.visibility = "visible";
  return;
}

function MostrarArbol(iSecElegido, iSecAncestro, sEspaciador){
  var aAncestros = new Array();
  var aDescendientes = new Array();
  
  if (iSecAncestro == null){
    //Si el ancestro no se indica es q vamos a procesar
    //el primer nivel.
    iSecAncestro = 0;
    aAncestros = QuitarDuplicados( BuscarAncestros( iSecElegido));
    aDescendientes = QuitarDuplicados( BuscarDescendientes( iSecElegido));
  } else {
    //Si el ancestro se indica es q vamos a procesar
    //los subniveles.
    aAncestros = QuitarDuplicados( BuscarAncestros( iSecAncestro));
    aDescendientes = QuitarDuplicados( BuscarDescendientes( iSecAncestro));
  }
    
  var sHTML = "";
  var i = 0;
  var iPosicion = 1;
  var oSecciones = document.getElementById('items');
  var aRama = FiltrarDescendencia(aRelaciones, iSecAncestro);
  var bEsAncestro = false;
  var bEsAncestroDirecto = false;
  var bEsAncestroPpal = false;
  var bEsDescendiente = false;
  var bEsDescendienteElegido = false;
  var bEsElElegido = false;
  var bEsCandidato = false;
  var bEsPariente = false;
  var bEsProcesable = false;
  var iAncestro = -1;
  var iDescendiente = -1;
  var iCaso = 0;
  var bUltimo = false;
  var sTexto = "";
  
  //if (iSecAncestro==3) debugger;
  if (aRama.length > 0){
    aRama.sort(OrdenarJerarquias);
    if (sEspaciador == null){
      // Estamos en el Nivel 0
      sEspaciador = "+";
    } else {
      //Estamos en un SubNivel
      //Lo primero es averiguar si tiene más de un descendiente
      switch (sEspaciador[sEspaciador.length-1]){
        case "+":
          // Si en el nivel anterior usamos un + ahora ponemos en su lugar un |
          sEspaciador = sEspaciador.substr(0,sEspaciador.length-1) + "|" ;
          break;
        case "L":
          // Si en el nivel anterior usamos un L ahora ponemos en su lugar un · (vacío)
          sEspaciador = sEspaciador.substr(0,sEspaciador.length-1) + "·" ;
          break;
      }
      if (aRama.length > 1){
        sEspaciador += "+";
      } else {
        sEspaciador += "L";
      }
    }
  }
  
  for (i=0; i < aRama.length; i++){
     iCaso = 0;
     iAncestro = aRama[i][0];
     iDescendiente = aRama[i][1];
     iPosicion = BuscarItem(oSecciones.options, iDescendiente);
     bEsElElegido = iDescendiente == iSecElegido;
     bEsAncestro = EstaEn(iDescendiente, aAncestros);
     bEsDescendiente = EstaEn(iDescendiente, aDescendientes);
     bEsDescendienteElegido = EsDescendienteDe(iDescendiente, iSecElegido);
     bEsPariente = bEsDescendiente || bEsAncestro;
     bEsAncestroPpal = iAncestro == 0;
     bEsCandidato = (!bEsElElegido && !bEsDescendienteElegido) || (bEsElElegido && !bEsPariente && !bEsAncestroPpal);
     bEsProcesable = (bEsElElegido) ||
                     (!bEsElElegido && (bEsDescendiente || bEsAncestroPpal));
     bEsAncestroDirecto = EsAncestroDe(iDescendiente, iSecElegido);
     bUltimo = ((i == aRama.length - 1) || (bEsAncestroPpal && iSecElegido == aRama[i+1][1] && i+1 == aRama.length - 1));
     if ( bUltimo){
       // Si es el último nivel donde usamos un + ahora ponemos en su lugar un L
       //sEspaciador = sEspaciador.substr(0,sEspaciador.length-1) + "L";
       switch (sEspaciador[sEspaciador.length-1]){
         case "+":
           // Si en el nivel anterior usamos un + ahora ponemos en su lugar un |
           sEspaciador = sEspaciador.substr(0,sEspaciador.length-1) + "L";
           break;
       /*  case "L":
           // Si en el nivel anterior usamos un L ahora ponemos en su lugar un · (vacío)
           sEspaciador = sEspaciador.substr(0,sEspaciador.length-1) + "·";
           break;*/
       }
     }
// Es procesable:
// - Es el Elegido:
//   Y No es Ancestro Ppal
// - Es Otro:
//   Y es Descendiente
// -------------------
// Es Candidato (Lleva CheckBox)
// - No es el Elegido
// - Es Ancestro
// - No es Descendiente
// -------------------
// Esta Marcado:
// - Si es Ancestro Directo
// -------------------
// Es Enunciable
     if (bEsProcesable){
       sTexto = oSecciones.options[iPosicion].text;
       sHTML += "<li id='ItemArbol'> ";
       if ( bEsCandidato){
          //armo el html para q muestre el checkbox o el elemento como hijo
          sHTML += "<label for='" + oSecciones.options[iPosicion].value + "' id='EtiquetaArbol'" +
                           "onmouseup='ModificarRelacion("+ iDescendiente + ", " + iSecElegido + ", \"ArbolItems\")'" + 
                   "> " +
                   PonerBrazo(sEspaciador) +
                   "<input type='checkbox' name='" + oSecciones.options[iPosicion].value + "' " +
                            "id='" + oSecciones.options[iPosicion].value + "'" + 
                            (bEsAncestroDirecto ? " checked='checked'/>":"/>")  +
                    sTexto + " " +
                   '</label> ';
       } else {
          // el elemento esta seleccionado
          // o es un descendiente (o ambas cosas)
          //if (!bEsAncestroPpal){
            //Si no es un Ancestro Ppal
            //if (bEsPariente) {
              //se muestra como descendiente
              //sin posibilidad de heredar. 
              //Con esto se evita ser hijo de si mísmo o padre de su padre
              //Le pongo un checkbox desabilitado asi "se ve que es hijo"
              sHTML += PonerBrazo(sEspaciador) + "<input type='checkbox' disabled='disabled'/>" + sTexto;
            //} 
          //}
       }
       //Ahora llamamos de nuevo a esta funcion
       //Para dibujar la descendencia del elemento en curso
       sHTML += MostrarArbol(iSecElegido, iDescendiente, sEspaciador);
       sHTML += '</li> ';
     }
  }
  if (sHTML != "") {
    //Encerramos la lista
     sHTML = "<ul id='ListaArbol' style='list-style-type: none; margin: 0; padding:0'> " + sHTML + "</ul> ";
  }
  return sHTML;
}

function QuitarDuplicados(aMatriz){
  var i = 0;
  var aSinDuplicados = new Array();
  var xAnt;
  
  for (i=0; i < aMatriz.length; i++){
    if (xAnt != aMatriz[i]) {
      aSinDuplicados.push(aMatriz[i]);
      xAnt = aMatriz[i];
    }
  }
  return aSinDuplicados;
}

function BuscarAncestros(iSecCodigo){
  var aAncestros = new Array();
  var i = 0;
  var aAscendencia = FiltrarAscendencia(aRelaciones, iSecCodigo);
  for (i = 0 ; i < aAscendencia.length ; i++){
      aAncestros.push(aAscendencia[i][0]);
      aAncestros = aAncestros.concat(BuscarAncestros(aAscendencia[i][0]));
  }
  return aAncestros.sort(OrdenarNumeros);
}

function BuscarDescendientes(iSecCodigo){
  var aDescendendientes = new Array();
  var i = 0;
  var aDescendencia = FiltrarDescendencia(aRelaciones, iSecCodigo);
  for (i = 0 ; i < aDescendencia.length ; i++){
      aDescendendientes.push(aDescendencia[i][1]);
      aDescendendientes = aDescendendientes.concat(BuscarDescendientes(aDescendencia[i][1]));
  }
  return aDescendendientes.sort(OrdenarNumeros);
}

function FiltrarAscendencia(aRama, iSecCodigo){
  var aFiltrado = new Array();
  var i = 0;
  for (i=0; i < aRama.length; i++){
    if (aRama [i][0] != 0 && aRama[i][1] == iSecCodigo){
      aFiltrado.push(aRama[i]);
    }
  }  
  return aFiltrado;
}

function FiltrarDescendencia(aRama, iSecAncestro){
  var aFiltrado = new Array();
  var i = 0;
  for (i=0; i < aRama.length; i++){
    if (aRama[i][0] == iSecAncestro){
      aFiltrado.push(aRama[i]);
    }
  }  
  return aFiltrado;
}

function BuscarItem(oObjeto, valor){
  var i = 0;
  for (i=0; i < oObjeto.length; i++){
     if (oObjeto[i].value == valor){
       break;
     }
  }
  return i;
}

function EstaEn(xBuscar, aMatriz){
  return  BuscarEn(xBuscar, aMatriz) != -1;
}

function BuscarEn(xBuscar, aMatriz){
  if (typeof(xBuscar) === 'undefined' || !aMatriz.length) return -1;
  var tope = aMatriz.length - 1;
  var piso = 0;
  while (piso <= tope) {
    indice = parseInt((piso + tope) / 2)
    elemento = aMatriz[indice];
    if (elemento > xBuscar) {
      tope = indice - 1;
    } else if (elemento < xBuscar) {
      piso = indice + 1;
    } else {
      return indice;
    }
  }
  return -1;
}

function EsDescendienteDe(iSecCodigo, iSecAncestro){
  var i;
  var bEsDescendienteDe = false;
  var aDescendientes = BuscarDescendientes(iSecAncestro);
  return EstaEn(iSecCodigo, aDescendientes);
}

function EsAncestroDe(iSecAncestro, iSecCodigo){
  var i;
  var bEsAncestroDe = false;
  for (i=0; i < aRelaciones.length; i++){
    if (aRelaciones[i][0] == iSecAncestro && aRelaciones[i][1] == iSecCodigo){
      bEsAncestroDe = true;
      break;
    }
  }
  return bEsAncestroDe;
}

function PonerBrazo( sEspaciador){
  var sHTML = "";
  var i = 0;
  for (i=0; i < sEspaciador.length; i++){
    switch (sEspaciador[i]){
      case "+":
        sHTML += "<img src='"+sDirImagenes+"/junta.gif' style='border: 0px;padding: 0px;height: 18px;vertical-align: absmiddle;'/>";
        break;
      case "L":
        sHTML += "<img src='"+sDirImagenes+"/brazo.gif' style='border: 0px;padding: 0px;height: 18px;vertical-align: absmiddle;'/>";
        break;
      case "|":
        sHTML += "<img src='"+sDirImagenes+"/paso.gif' style='border: 0px;padding: 0px;height: 18px;vertical-align: absmiddle;'/>";
        break;
      case "·":
        sHTML += "<img src='"+sDirImagenes+"/vacio.gif' style='border: 0px;padding: 0px;height: 18px;vertical-align: absmiddle;'/>";
        break;
    }
  }
  return sHTML;
}

function CambiarPosicion(sObjeto, sDireccion, sIdentificacion){
  var oObjeto = document.getElementById(sObjeto);
  var iPosicionActual = parseInt(oObjeto.selectedIndex);
  var iPosicionNueva = 0;
  var i = 0;
  var j = 0;
    
  switch (sDireccion){
    case "pri":
      iPosicionNueva = 0;
      break;
    case "ant":
      iPosicionNueva = iPosicionActual-1;
      break;
    case "pos":
      iPosicionNueva = iPosicionActual+1;
      break;
    case "ult":
      iPosicionNueva = aItems.length-1;
      break;
  }
  if (sDireccion == "pri" || sDireccion == "ant"){
    for (i=iPosicionActual; i > iPosicionNueva; i--){
      aItems[i-1][2] = aItems[iPosicionActual][2];
      aItems[iPosicionActual][2] -= 1;
    }
  } else {
    for (i=iPosicionActual; i < iPosicionNueva; i++){
      aItems[i+1][2] = aItems[iPosicionActual][2];
      aItems[iPosicionActual][2] += 1;
    }
  }
  for (i=0; i < aItems.length; i++){
    for (j=0; j < aRelaciones.length; j++){
      if (aItems[i][0] == aRelaciones[j][1]){
        aRelaciones[j][2] = aItems[i][2];
      }
    }
  }
  document.getElementById("pri").disabled = iPosicionNueva == 0;
  document.getElementById("ant").disabled = iPosicionNueva == 0;
  document.getElementById("pos").disabled = iPosicionNueva == aItems.length-1;
  document.getElementById("ult").disabled = iPosicionNueva == aItems.length-1;
  document.getElementById("Guardar").disabled = false;
  ActualizarLista("jer",sObjeto, iPosicionNueva);
  ActualizarArbol(document.getElementById(sObjeto).options[document.getElementById(sObjeto).selectedIndex].value,sIdentificacion);
}

function ModificarRelacion(iSecAncestro, iSecCodigo, sIdentificacion){
  var i = 0;
  var iRelaciones = 0;
  var iIndiceComoPadre = -1;
  var iIndiceComoHijo = -1;
  var bEstaRelacionado = EsAncestroDe(iSecAncestro, iSecCodigo);
  
  if (!bEstaRelacionado) {
    // Verifico si existe com única relación
    // de ser asi, modifico el Padre
    for (i=0; i < aRelaciones.length; i++){
      if (aRelaciones[i][1] == iSecCodigo){
        iRelaciones++;
        if (aRelaciones[i][0] == 0) {
          iIndiceComoPadre = i;
        } else if (aRelaciones[i][0] == iSecAncestro) {
          //Ya existe, cuando termine debe quitarse este código
          // xq no se va a dar esta posibilidad
          alert('Ya Existe!');
          return;
        }
      }
    }
    if (iRelaciones == 1 && iIndiceComoPadre > -1) {
      aRelaciones[iIndiceComoPadre][0] = iSecAncestro;
    } else {
      // Sino agrego nueva relacion
      aRelaciones.push( new Array(iSecAncestro, iSecCodigo));
    }
  } else {
    for (i=0; i < aRelaciones.length; i++){
      if (aRelaciones[i][1] == iSecCodigo){
        iRelaciones++;
        if (aRelaciones[i][0] == 0) {
          iIndiceComoPadre = i;
        }
        if (aRelaciones[i][0] == iSecAncestro && aRelaciones[i][1] == iSecCodigo ){
          iIndiceComoHijo = i;
        }
      }
    }
    if (iRelaciones == 1 && iIndiceComoHijo > -1){
      aRelaciones[iIndiceComoHijo][0] = 0;
    } else {
      aRelaciones.splice(iIndiceComoHijo, 1);
    }
  }
  aRelaciones.sort(OrdenarRelaciones);
  ActualizarArbol(iSecCodigo,sIdentificacion);
  document.getElementById("Guardar").disabled = false;
  return;
}

function OrdenarRelaciones(aItem1, aItem2){
  if (aItem1[0] < aItem2[0]){
    return -1;
  } else if (aItem1[0] == aItem2[0]) {
     return aItem1[2] - aItem2[2];
  } else if (aItem1[0] > aItem2[0]) {
    return 1;
  }
}
