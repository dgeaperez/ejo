<?php

/**
 * This is the model class for table "tbl_ejo".
 *
 * The followings are the available columns in table 'tbl_item':
 * @property integer $itmCodigo
 * @property string $idiCodigo
 * @property string $idiDescripcion
 * @property string $itmDescripcion
 */
class Item extends CActiveRecord {
  //ITEM MAIN TABLE STRUCTURE

  const ITEM_MAIN_TABLE_STRUCTURE = <<<FIN
CREATE TABLE `TABLE_NAME` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `itmCodigo` int(11) NOT NULL,
 `itmActivo` tinyint(1) DEFAULT '0',
 `idiCodigo` varchar(45) NOT NULL,
 `itmDescripcion` varchar(45) NOT NULL,
 `itmOrden` int(11) DEFAULT '0',
 `itmBaja` datetime DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `idiCodigo` (`idiCodigo`),
 KEY `codigo` (`itmCodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
FIN;
  const ITEM_MAIN_TABLE_DATA = <<<FIN
INSERT INTO `TABLE_NAME` 
  (`id`, `itmCodigo`, `itmActivo`, `idiCodigo`, `itmDescripcion`, `itmOrden`, `itmBaja`) 
VALUES
  (1, 0, 1, 'LANGUAGE', 'raiz', 0, NULL);
FIN;
  const ITEM_RELATIONS_TABLE_STRUCTURE = <<<FIN
CREATE TABLE `TABLE_NAME` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `itmAncestro` int(11) NOT NULL,
 `itmCodigo` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `itmCodigoAnc` (`itmAncestro`),
 KEY `itmCodigoDes` (`itmCodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
FIN;
  //TODO WHEN GEMI IS NOT INSTALLED tbl_idioma DOESN'T EXISTS!
  const ITEM_MAIN_VIEW_STRUCTURE_WITH_GEMI = <<<FIN
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `VIEW_NAME` AS 
  select if(isnull(`MAIN_TABLE`.`itmCodigo`),-(1),`MAIN_TABLE`.`itmCodigo`) AS `itmCodigo`,
  `tbl_idioma`.`idiCodigo` AS `idiCodigo`,
  `tbl_idioma`.`idiDescripcion` AS `idiDescripcion`,
  if(isnull(`MAIN_TABLE`.`itmDescripcion`),'',
  `MAIN_TABLE`.`itmDescripcion`) AS `itmDescripcion` 
  from (`tbl_idioma` 
     left join `MAIN_TABLE` 
     on((`MAIN_TABLE`.`idiCodigo` = `tbl_idioma`.`idiCodigo`))) 
  order by `tbl_idioma`.`idiDescripcion`
FIN;
  const ITEM_RELATIONS_VIEW_STRUCTURE_WITH_GEMI = <<<FIN
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `VIEW_NAME` AS 
  select `RELATION_TABLE`.`itmAncestro` AS `itmAncestro`,
  `MAIN_TABLE`.`itmOrden` AS `itmOrden`,
  `MAIN_TABLE`.`itmCodigo` AS `itmCodigo`,
  `MAIN_TABLE`.`itmDescripcion` AS `itmDescripcion`,
  `MAIN_TABLE`.`idiCodigo` AS `idiCodigo` 
  from (`MAIN_TABLE` 
    join `RELATION_TABLE` 
     on((`MAIN_TABLE`.`itmCodigo` = `RELATION_TABLE`.`itmCodigo`))) 
  where ((`MAIN_TABLE`.`idiCodigo` = 'SL') 
    and isnull(`MAIN_TABLE`.`itmBaja`)) 
  order by `RELATION_TABLE`.`itmAncestro`,`MAIN_TABLE`.`itmOrden`
FIN;
  const ITEM_RELATIONS_VIEW_STRUCTURE_WITHOUT_GEMI = <<<FIN
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `VIEW_NAME` AS 
  select `RELATION_TABLE`.`itmAncestro` AS `itmAncestro`,
  `MAIN_TABLE`.`itmOrden` AS `itmOrden`,
  `MAIN_TABLE`.`itmCodigo` AS `itmCodigo`,
  `MAIN_TABLE`.`itmDescripcion` AS `itmDescripcion`,
  `MAIN_TABLE`.`idiCodigo` AS `idiCodigo` 
  from (`MAIN_TABLE` 
    join `RELATION_TABLE` 
     on((`MAIN_TABLE`.`itmCodigo` = `RELATION_TABLE`.`itmCodigo`))) 
  where isnull(`MAIN_TABLE`.`itmBaja`) 
  order by `RELATION_TABLE`.`itmAncestro`,`MAIN_TABLE`.`itmOrden`
FIN;

  public static $_mainTable = '';
  public static $_relationsTable = '';
  public static $_relationsView = '';
  public static $_mainView = '';
  public static $_codeMsg = '';
  public static $_titleSingular = '';
  public static $_titlePlural = '';
  public static $_route = '';
  public static $_gemiInstalled = false;
  public static $_sl = '';

  /**
   * Returns the static model of the specified AR class.
   * @return Item the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    $this->verificarTabla();
    return self::$_mainTable;
  }

  public function verificarTabla() {
    /* Versión Original
     * 
     */
    /* if (!self::$_initiated)
      $this->init(); */

    $_database = Yii::app()->db
            ->createCommand('SELECT DATABASE()')
            ->queryScalar();

    //Checking Main Table
    $tbl_existe = Yii::app()->db
                    ->createCommand(
                            "SELECT COUNT(*) AS Existe " .
                            "FROM information_schema.tables " .
                            "WHERE table_schema = '$_database'" .
                            "AND table_name = '" . self::$_mainTable . "'")
                    ->queryScalar() > 0;

    if (!$tbl_existe) {
      $sql = str_replace('TABLE_NAME', self::$_maintable, self::ITEM_MAIN_TABLE_STRUCTURE);
      Yii::app()->db->createCommand($sql)->execute();

      $sql = str_replace(
              array('TABLE_NAME', 'LANGUAGE'), array(self::_mainTable, Yii::app()->language), self::ITEM_MAIN_TABLE_DATA);
      Yii::app()->db->createCommand($sql)->execute();
    }

    //Checking Relations Table
    $tbl_existe = Yii::app()->db
                    ->createCommand(
                            "SELECT COUNT(*) AS Existe " .
                            "FROM information_schema.tables " .
                            "WHERE table_schema = '$_database' " .
                            "AND table_name = '" . self::$_relationsTable . "'")
                    ->queryScalar() > 0;

    if (!$tbl_existe) {
      $sql = str_replace(
              'TABLE_NAME', self::$_maintable, self::ITEM_RELATIONS_TABLE_STRUCTURE);
      Yii::app()->db->createCommand($sql)->execute();
    }

    //Checking Main View
    $vst_existe = Yii::app()->db
                    ->createCommand(
                            "SELECT COUNT(*) AS Existe " .
                            "FROM information_schema.tables " .
                            "WHERE table_schema = '$_database' " .
                            "AND table_name = '" . self::$_mainView . "'")
                    ->queryScalar() > 0;

    if (!$vst_existe) {
      $sql = str_replace(
              array('VIEW_NAME', 'MAIN_TABLE'), array(self::$_mainView, self::_mainTable), self::ITEM_MAIN_VIEW_STRUCTURE_WITH_GEMI
      );
      Yii::app()->db->createCommand($sql)->execute();
    }

    //Checking Relations View
    $vst_existe = Yii::app()->db
                    ->createCommand(
                            "SELECT COUNT(*) AS Existe " .
                            "FROM information_schema.tables " .
                            "WHERE table_schema = '$_database' " .
                            "AND table_name = '" . self::$_relationsView . "'")
                    ->queryScalar() > 0;

    if (!$vst_existe) {
      $sql = str_replace(
              array('VIEW_NAME', 'MAIN_TABLE', 'RELATION_TABLE', 'SL'), array(self::$_mainView, self::_mainTable, self::$_relationsTable, self::$_sl), self::ITEM_RELATIONS_VIEW_STRUCTURE_WITH_GEMI
      );
      Yii::app()->db->createCommand($sql)->execute();
    }

    //Now, checking if a previous view exist and not be there dpending the
    //GeMI existance
    if (self::$_gemiInstalled) {
      if (substr(self::$_relationsView, -5) === '_GeMI')
        $oldViewName = substr(self::$_relationsView, 0, strlen(self::$_relationsView) - 5);
    } else {
      if (substr(self::$_relationsView, -5) !== '_GeMI')
        $oldViewName = self::$_relationsView . '_GeMI';
    }

    Yii::app()->db
            ->createCommand("DROP VIEW IF EXISTS $oldViewName")
            ->execute();

    /* Add here the subsequent modifications and the procedures to update 
     * the tables/views strutures
     */
  }

  public function titulo($i = 0) {
    return Yii::t("int_TIT_" . self::$_codeMsg, self::$_titleSingular . '|' . self::$_titlePlural, $i);
  }

  /**
   * @return value or array with the associated primary key
   */
  public function primaryKey() {
    //return 'itmCodigo';
    //return array('id'); //No recuerdo xq lo dejé con uno solo
    return array('itmCodigo', 'idiCodigo');
    // For composite primary key, return an array like the following
    // return array('pk1', 'pk2');
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
        array('itmDescripcion', 'length', 'max' => 30),
        array('itmDescripcion', 'SinRepetir', 'on' => 'insert, update'),
        // The following rule is used by search().
        // Please remove those attributes that should not be searched.
        array('idiDescripcion, itmDescripcion', 'safe', 'on' => 'search'),
        array('itmDescripcion', 'Obligatorio', 'on' => 'insert, update'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    $relationsForGemi = array();
    if (self::$_gemiInstalled) {
      $relationsForGemi = array(
          'tblIdioma' => array(self::BELONGS_TO, 'Idioma', 'idiCodigo'),
          'idiomas' => array(self::BELONGS_TO, 'Idioma', 'idiCodigo'),
          'idiCodigo' => array(self::BELONGS_TO, 'Idioma', 'idiCodigo'),
      );
    }

    return array_merge($relationsForGemi, array(
                'ancestro' => array(self::HAS_MANY, self::$_relationsTable, 'itmCodigo'),
                'descendientes' => array(self::HAS_MANY, self::$_relationsTable, 'itmAncestro', 'order' => 'itmOrden ASC'),
                'hayRegistros' => array(self::STAT, self::$_mainTable, 'itmCodigo'),
                    )
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    $atributos = array();
    if (self::$_gemiInstalled) {
      $idiomas = Yii::app()->getModule('gemi')->ListarIdiomasVigentes;
      foreach ($idiomas as $idiCodigo => $idiDescripcion) {
        $atributos["{$idiCodigo}_idiDescripcion"] = $idiDescripcion;
      }
    }


    return array_merge($atributos, array(
                //'itmCodigo' => 'Código',
                'itmOrden' => '',
                'itmDescripcion' => Yii::t('atr_' . self::$_codeMsg . '_ITMDESCRIPCION', 'Descripción'),
                'itmActivo' => Yii::t('atr_' . self::$_codeMsg . '_ITMACTIVO', 'Activado'),
                'idiDescripcion' => Yii::t('atr_' . self::$_codeMsg . '_IDIDESCRIPCION', 'Idioma'),
            ));
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search() {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.

    $criteria = new CDbCriteria;

    //$criteria->compare('itmCodigo',$this->itmCodigo);
    //$criteria->compare('idiDescripcion', $this->idiDescripcion, true);

    $criteria->compare('itmDescripcion', $this->itmDescripcion, true);
    if (!self::$_gemiInstalled)
      $criteria->condition = 'itmCodigo != 0';

    return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
  }

  /*
    public function defaultScope() {
    return array('condition' => 'itmCodigo != 0',);
    }
   */

  public function scopes() {
    $sourceLang = Yii::app()->sourceLanguage;
    return array(
        'no_sourceLang' => array(
            'condition' => "idiCodigo != '$sourceLang' and itmCodigo != 0",
        ),
        'sourceLang' => array(
            'condition' => "idiCodigo='$sourceLang' and itmCodigo != 0",
        ),
    );
  }

  //reemplazado por $model->hayRegistros
  //public function getHaySecciones() {
  //  return Yii::app()->db->createCommand("SELECT COUNT(*) FROM self::_mainTable")->queryScalar() > 1;
  //}

  public function SinRepetir($a, $p) {
    if (self::$_gemiInstalled)
      self::SinRepetirConGemi();
    else
      self::SinRepetirSinGemi();
  }

  public function SinRepetirConGemi() {
    $mt = self::$_mainTable;
    $sSQL = <<<EOT
SELECT tbl_idioma.idiDescripcion 
    FROM ($mt join tbl_idioma on(($mt.idiCodigo = tbl_idioma.idiCodigo)))  
    WHERE $mt.idiCodigo='$this->idiCodigo' 
      AND $mt.itmDescripcion LIKE '$this->itmDescripcion'
EOT;
    if ($this->itmCodigo != -1) {
      //el codigo esta asignado, es una actualizacion si esta repetido es xq es otro codigo
      $sSQL .= "AND $mt.itmCodigo != '{$this->itmCodigo}'";
    }
    $aRegistros = Yii::app()->db->createCommand($sSQL)->queryAll();
    if ($aRegistros) {
      $this->addError($this->idiCodigo . '_itmDescripcion', Yii::t('err_REPETIDO', '{data} está en uso para {data2}', array('{data}' => $this->itmDescripcion, '{data2}' => $aRegistros[0]['idiDescripcion'])));
    }
  }

  public function SinRepetirSinGemi() {
    $hayRegistros = $this->findAllByAttributes(array('itmDescripcion' => $this->itmDescripcion));
    if ($hayRegistros)
      $this->addError('itmDescripcion', Yii::t('err_REPETIDO2', '{data} está en uso', array('{data}' => $this->itmDescripcion)));
  }

  public function Obligatorio($a, $p) {
    if (empty($this->itmDescripcion) && $this->idiCodigo == Yii::app()->sourceLanguage) {
      $this->addError($this->idiCodigo . '_itmDescripcion', Yii::t('err_VACIO', '{attribute} no puede estar vacío.', array('{attribute}' => $this->idiDescripcion)));
    }
  }

  public function getMainTable() {
    return self::_mainTable;
  }

  public function getRelationsTable() {
    return $this->_relationsTable;
  }

  public function getCodeMsg() {
    return $this->_codeMsg;
  }
}