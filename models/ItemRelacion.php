<?php

/**
 * This is the model class for table "vst_menuGenericoRelacion".
 *
 * The followings are the available columns in table 'tbl_idioma':
 * @property string $idiCodigo
 * @property string $descripcion
 * @property integer $activo
 */
class ItemRelacion extends CActiveRecord {

  public static $_mainTable = '';
  public static $_relationsTable = '';
  public static $_relationsView = '';
  public static $_mainView = '';
  public static $_codeMsg = '';
  public static $_titleSingular = '';
  public static $_titlePlural = '';
  public static $_titleHierEdit = '';
  public static $_route = '';
  public static $_gemiInstalled = false;

  /**
   * Returns the static model of the specified AR class.
   * @return Idioma the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    /*if (!$this->_initiated)
      $this->init();*/
    return self::$_relationsView;
  }

  public function titulo($i = 0) {
    return Yii::t('int_TIT_' . self::$_codeMsg, self::$_titleSingular . '|' . self::$_titlePlural, $i);
  }

  /**
   * @return value or array with the associated primary key
   */
  public function primaryKey() {
    //return 'itmCodigo';
    return array('itmCodigo', 'idiCodigo'); //No recuerdo xq lo dejé con uno solo
    // For composite primary key, return an array like the following
    // return array('pk1', 'pk2');
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
        array('itmAncestro', 'numerical', 'integerOnly' => true),
        array('itmOrden', 'numerical', 'integerOnly' => true),
        array('itmCodigo', 'numerical', 'integerOnly' => true),
        array('itmDescripcion', 'length', 'max' => 60),
        array('idiCodigo', 'length', 'max' => 5),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            //array('idiCodigo, idiDescripcion, idiActivo', 'safe', 'on'=>'search'),
            //array('idiCodigo, idiDescripcion', 'required'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
        'items' => array(self::HAS_MANY, "Item", 'itmCodigo'),
        'ancestro' => array(self::BELONGS_TO, "ItemRelacion", 'itmAncestro'),
        'descendientes' => array(self::HAS_MANY, "ItemRelacion", 'itmAncestro', 'order' => 'itmOrden ASC'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
        'itmDescripcion' => 'Ítem',
    );
  }

  public function getRutasPosibles($iSemilla = 0, $sRuta = '0', $sCamino = "/") {
    $subItems = array();
    $descendientes = $this->findAllByAttributes(array('itmAncestro' => $iSemilla));
    $sIdioma = Yii::app()->language;
    $mt = self::$_mainTable;
    $sourceLang = Yii::app()->sourceLanguage;
    $sSQL = <<<EOT
SELECT t1.itmCodigo as itmCodigo, 
       IFNULL((SELECT t2.itmDescripcion
    		 FROM $mt t2
		     WHERE (t2.itmCodigo=t1.itmCodigo AND
		            t2.idiCodigo='$sIdioma')), t1.itmDescripcion) as itmDescripcion
FROM $mt t1 
WHERE t1.idiCodigo = '$sourceLang' and t1.itmCodigo = $iSemilla
EOT;
    $aSeccion = Yii::app()->db->createCommand($sSQL)->queryAll();
    if ($iSemilla != 0)
      $sCamino .= $aSeccion[0]['itmDescripcion'] . '/';
    if (count($descendientes) > 0) {
      foreach ($descendientes as $descendiente) {
        $subItems = array_merge($subItems, $descendiente->getRutasPosibles($descendiente->itmCodigo, $sRuta . ',' . $descendiente->itmCodigo, $sCamino));
      }
    }
    if ($iSemilla != 0) {
      //Sólo procesaremos los descendientes  
      $menu[$sRuta] = $sCamino;
      if ($subItems != array())
        $menu = array_merge($menu, $subItems);
      return $menu;
    } else {
      //salimos de la iteracion y la recursividad
      // subItems tiene la estructura armada
      return $subItems;
    }
  }

  public function getRutasDisponibles($idioma = null, $incluir = '') {
    if ($idioma === null)
      $idioma = Yii::app()->sourceLanguage;
    $aRutas = array();
    $aRutasPosibles = $this->getRutasPosibles();
    if (!empty($incluir))
      $aRutas[$incluir] = $aRutasPosibles[$incluir];
    foreach ($aRutasPosibles as $sRuta => $sRutaCompleta) {
      if (count(Contenido::model()->findByAttributes(array('conSeccion' => $sRuta, 'idiCodigo' => $idioma))) == 0) {
        $aRutas[$sRuta] = $sRutaCompleta;
      }
    }
    return $aRutas;
  }

  public function getTraducirRuta($sRuta = '') {
    $aRamas = explode(',', $sRuta);
    $sCamino = '/';
    unset($aRamas[0]);
    foreach ($aRamas as $v => $k) {
      $aSeccion = Seccion::model()->findByAttributes(array('itmCodigo' => $k, 'idiCodigo' => Yii::app()->language));
      if (count($aSeccion) == 0 || empty($aSeccion->itmDescripcion)) {
        $aSeccion = Seccion::model()->findByAttributes(array('itmCodigo' => $k, 'idiCodigo' => Yii::app()->sourceLanguage));
      }
      $sCamino .= $aSeccion->itmDescripcion . '/';
    }
    return $sCamino;
  }

}