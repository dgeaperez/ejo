<?php

class ItemController extends Controller {

  /**
   * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
   * using two-column layout. See 'protected/views/layouts/column2.php'.
   */
  public $layout = '//layouts/column2';

  /**
   * @var CActiveRecord the currently loaded data model instance.
   */
  private $_model;
  private $_name;
  private $_mainTable;
  private $_relationsTable;
  private $_gemiInstalled = false;
  private $_codeMsg = '';
  private $_titleSingular = '';
  private $_titlePlural = '';
  private $_titleIndex = '';
  private $_titleCreate = '';
  private $_titleUpdate = '';
  private $_titleDelete = '';
  private $_titleView = '';
  private $_titleHierEdit = '';
  private $_initiated = false;
  private $_sl = '';
  private $_slDescripcion = '';

  public function init() {
    $this->_name = ucfirst($this->module->name);
    $this->_mainTable = $this->module->mainTable;
    $this->_relationsTable = $this->module->relationsTable;
    $this->_gemiInstalled = $this->module->gemiInstalled;
    $this->_codeMsg = $this->module->codeMsg;
    $this->_titleSingular = $this->module->titleSingular;
    $this->_titlePlural = $this->module->titlePlural;
    $this->_titleIndex = $this->module->titleIndex;
    $this->_titleCreate = $this->module->titleCreate;
    $this->_titleUpdate = $this->module->titleUpdate;
    $this->_titleDelete = $this->module->titleDelete;
    $this->_titleView = $this->module->titleView;
    $this->_titleHierEdit = $this->module->titleHierEdit;
    $this->_sl = $this->module->sl;
    $this->_slDescripcion = $this->module->slDescripcion;
    parent::init();
    $this->_initiated = true;
  }

  /**
   * @return array action filters
   */
  public function filters() {
    return array(
        'accessControl', // perform access control for CRUD operations
    );
  }

  /**
   * Specifies the access control rules.
   * This method is used by the 'accessControl' filter.
   * @return array access control rules
   */
  public function accessRules() {
    return array(
        array('allow', // allow all users to perform 'index' and 'view' actions
            'actions' => array('index', 'view'),
            'users' => array('*'),
        ),
        array('allow', // allow authenticated user to perform 'create' and 'update' actions
            'actions' => array('create', 'update', "disponiblesAjax"),
            'users' => array('@'),
        ),
        array('allow', // allow admin user to perform 'admin' and 'delete' actions
            'actions' => array('admin', 'delete', 'connect', 'acceso', 'ajaxFillTree', 'simpleTree'),
            'users' => array('admin'),
        ),
        array('deny', // deny all users
            'users' => array('*'),
        ),
    );
  }

  /**
   * Displays a particular model.
   */
  public function actionView() {
    $itemsEspanol = new CActiveDataProvider("Item", array(
                'criteria' => array(
                    'condition' => 'idiCodigo = \'es\' AND itmCodigo != 0',
                    'params' => array(),
                ),
                'pagination' => array(
                    'pageSize' => 1,
                ),
            ));

    $this->render('view', array(
        'model' => $this->loadModel(),
        'itemsEspanol' => $itemsEspanol,
    ));
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * This is a wrapper from the original, because the logic is a little bit different
   * when GeMI is present or not
   */
  public function actionCreate() {
    if ($this->_gemiInstalled)
      self::CreateWithGemiSupport();
    else
      self::CreateWithoutGemiSupport();
  }

  private function CreateWithGemiSupport() {
    $sSQL = <<<EOT
SELECT 
IF(ISNULL($this->_mainTable.itmCodigo),-1, $this->_mainTable.itmCodigo) AS itmCodigo, 
$this->_mainTable.itmOrden, 
$this->_mainTable.itmActivo, 
tbl_idioma.idiCodigo, 
tbl_idioma.idiDescripcion, 
IF(ISNULL($this->_mainTable.itmDescripcion),'',$this->_mainTable.itmDescripcion) AS itmDescripcion 
FROM $this->_mainTable
RIGHT OUTER JOIN tbl_idioma
  ON ($this->_mainTable.idiCodigo = tbl_idioma.idiCodigo) 
  AND ($this->_mainTable.itmCodigo = :itmCodigo)
WHERE (tbl_idioma.idiBaja IS NULL)
ORDER BY tbl_idioma.idiDescripcion
EOT;
    // retrieve items to be updated in a batch mode
    // assuming each item is of model class 'Item'

    $model = new Item;

    $registros = $model->findAllBySql($sSQL, array(':itmCodigo' => 0));
    foreach ($registros as $i => $item) {
      $items[$item->idiCodigo] = new Item;
      $items[$item->idiCodigo]->setAttributes($item->getAttributes(), false);
    }
    $sl = Yii::app()->sourceLanguage;
    $item = $items[$this->_sl];
    unset($items[$this->_sl]);
    $items = array($this->_sl => $item) + $items;
    $items[$this->_sl]->itmDescripcion = '';
    unset($registros, $i, $item);

    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($items);

    if (isset($_POST["Item"])) {
      $sSQL = "select MAX(itmCodigo) from $this->_mainTable;";
      $itmCodigoNuevo = Yii::app()->db->createCommand($sSQL)->queryScalar() + 1;
      $itmOrdenNuevo = $itmCodigoNuevo;
      $todos_ok = true;
      foreach ($items as $i => $item) {
        if ($_POST["Item"][$i]['itmDescripcion'] != "") {
          $item->itmCodigo = $itmCodigoNuevo;
          $item->itmOrden = $itmOrdenNuevo;
          $item->idiCodigo = $i;
          $item->itmDescripcion = $_POST["Item"][$i]['itmDescripcion'];
          $item->itmActivo = $_POST["Item"]['itmActivo'];
          $esta_ok = $item->validate();
          $todos_ok = $todos_ok && $esta_ok;
        }
      }

      if ($todos_ok) {
        foreach ($items as $i => $item) {
          if ($item->itmDescripcion != "") {
            $datos['itmCodigo'] = $itmCodigoNuevo;
            $datos['itmActivo'] = $item->itmActivo;
            $datos['idiCodigo'] = $item->idiCodigo;
            $datos['itmDescripcion'] = $item->itmDescripcion;
            $datos['itmOrden'] = $item->itmOrden;
            $sSQL = Yii::app()->db->commandBuilder->createInsertCommand($this->_mainTable, $datos)->execute();
          }
        }
        unset($datos);
        //actualizo las relaciones
        $datos['itmCodigo'] = $itmCodigoNuevo;
        $datos['itmAncestro'] = 0;
        $sSQL = Yii::app()->db->commandBuilder->createInsertCommand($this->_relationsTable, $datos)->execute();

        if ($this->module->afterCreate !== '')
          eval($this->module->afterCreate . "($itmCodigoNuevo);");

        $this->redirect(array('index'));
      }
    }
    $this->render('create', array(
        'model' => $items,
    ));
  }

  private function CreateWithoutGemiSupport() {
    $model = new Item;
    $model->setAttributes($model->getAttributes(), false);
    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model);

    if (isset($_POST['Item'])) {
      $model->attributes = $_POST['Item'];
      $model->itmActivo = $_POST['Item']['itmActivo'];
      $model->idiCodigo = Yii::app()->language;
      $model->itmCodigo = $this->proximoCodigo();
      //El orden en este momento es indiferente, y siempre irá a parar al último
      $model->itmOrden = $model->itmCodigo;
      if ($model->save()) {
        //si lo guardó, lo agregamos a la tabla de relaciones pendiendo del raíz
        $datos['itmCodigo'] = $model->itmCodigo;
        $datos['itmAncestro'] = 0;
        Yii::app()->db->commandBuilder
                ->createInsertCommand($this->_relationsTable, $datos)
                ->execute();

        if ($this->module->afterCreate !== '')
          eval($this->module->afterCreate . "($model->itmCodigo);");

        $this->redirect(array('index'));
      }
    }

    $this->render('create', array(
        'model' => $model,
    ));
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'view' page.
   */
  public function actionUpdate() {
    if ($this->_gemiInstalled)
      self::UpdateWithGemiSupport();
    else
      self::UpdateWithoutGemiSupport();
  }

  public function UpdateWithGemiSupport() {
    if ($_GET['id'] == '0') {
      throw new CHttpException(400, Yii::t('err_CCONTROLLER_400', 'Solicitud Incorrecta. Por favor no la repita.'));
    }
    $model = $this->loadModel();
    $sSQL = <<<EOT
SELECT 
IF(ISNULL($this->_mainTable.itmCodigo),-1, $this->_mainTable.itmCodigo) AS itmCodigo, 
$this->_mainTable.itmActivo, 
$this->_mainTable.itmOrden, 
tbl_idioma.idiCodigo, 
tbl_idioma.idiDescripcion, 
IF(ISNULL($this->_mainTable.itmDescripcion),'',$this->_mainTable.itmDescripcion) AS itmDescripcion 
FROM $this->_mainTable
RIGHT OUTER JOIN tbl_idioma
  ON ($this->_mainTable.idiCodigo = tbl_idioma.idiCodigo) 
  AND ($this->_mainTable.itmCodigo = :itmCodigo)
WHERE (tbl_idioma.idiBaja IS NULL)
ORDER BY tbl_idioma.idiDescripcion
EOT;
    // retrieve items to be updated in a batch mode
    // assuming each item is of model class 'Item'
    //$model = new Item;

    $registros = Item::model()->findAllBySql($sSQL, array(':itmCodigo' => $model->itmCodigo));

    foreach ($registros as $i => $item) {
      $items[$item->idiCodigo] = $item;
    }
    $item = $items[$this->_sl];
    unset($items[$this->_sl]);
    $items = array_merge(array($this->_sl => $item), $items);
    unset($registros, $i, $item);

    $this->performAjaxValidation($items);

    if (isset($_POST["Item"])) {
      $todos_ok = true;
      foreach ($items as $i => $item) {
        $item->itmCodigo = $_POST["Item"][$i]['itmCodigo'];
        $item->itmActivo = $_POST["Item"]['itmActivo'];
        $item->itmOrden = $items[$this->sl]->itmOrden;
        $item->idiCodigo = $i;
        $item->itmDescripcion = $_POST["Item"][$i]['itmDescripcion'];
        $esta_ok = $item->validate();
        $todos_ok = $todos_ok && $esta_ok;
      }

      if ($todos_ok) {
        //Ningún item tiene error
        foreach ($items as $i => $item) {
          if ($item->itmDescripcion != "") {
            //El _name del Seccion se indicó
            $datos['idiCodigo'] = $item->idiCodigo;
            $datos['itmDescripcion'] = $item->itmDescripcion;
            $datos['itmOrden'] = (int) $item->itmOrden;
            if ($item->itmCodigo == '-1') {
              //El código no estaba seteado por lo q es una nueva traducción
              $datos['itmCodigo'] = $items[$this->_sl]->itmCodigo;
              $datos['itmActivo'] = $items[$this->_sl]->itmActivo;
              $sSQL = Yii::app()->db->commandBuilder->createInsertCommand($this->_mainTable, $datos)->execute();
            } else {
              //El código está indicado, es una actualización
              $datos['itmCodigo'] = $item->itmCodigo;
              $datos['itmActivo'] = $items[$this->_sl]->itmActivo;
              $criterio = new CDbCriteria;
              $criterio->condition = 'itmCodigo=:itmCodigo AND idiCodigo=:idiCodigo';
              $criterio->params = array(':itmCodigo' => $item->itmCodigo,
                  ':idiCodigo' => $item->idiCodigo);
              $sSQL = Yii::app()->db->commandBuilder->createUpdateCommand($this->_mainTable, $datos, $criterio)->execute();
            }
          } else if ($item->itmCodigo != '-1') {
            //El _name está vacío y tiene código, se eliminó esa traducción  
            $criterio = new CDbCriteria;
            $criterio->condition = 'itmCodigo=:itmCodigo AND idiCodigo=:idiCodigo';
            $criterio->params = array(':itmCodigo' => $item->itmCodigo,
                ':idiCodigo' => $item->idiCodigo);
            $sSQL = Yii::app()->db->commandBuilder->createDeleteCommand($this->_mainTable, $criterio)->execute();
          }
        }
        unset($datos);

        if ($this->module->afterUpdate !== '')
          eval($this->module->afterUpdate . "($model->itmCodigo);");

        $this->redirect(array('index'));
      }
    }

    // displays the view to collect tabular input
    $this->render('update', array('model' => $items));
  }

  public function UpdateWithoutGemiSupport() {
    $model = $this->loadModel();

    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model);

    if (isset($_POST['Item'])) {
      $model->attributes = $_POST['Item'];
      $model->itmCodigo = $model->itmCodigo;
      $model->itmActivo = $_POST['Item']['itmActivo'];
      if ($model->save()) {
        if ($this->module->afterUpdate !== '')
          eval($this->module->afterUpdate . "($model->itmCodigo);");

        $this->redirect(array('index'));
      }
    }

    $this->render('update', array(
        'model' => $model,
    ));
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   */
  public function actionDelete() {
    $model = $this->loadModel();
    if (Yii::app()->request->isPostRequest && $model->itmCodigo != '0') {
      // we only allow deletion via POST request
      $criterio = new CDbCriteria;
      // Actualizo las relaciones q lo tienen como 'padre'
      $datos['itmAncestro'] = 0;
      $criterio->condition = 'itmAncestro=:itmCodigo';
      $criterio->params = array(':itmCodigo' => $model->itmCodigo);
      $sSQL = Yii::app()->db->commandBuilder->createUpdateCommand($this->_relationsTable, $datos, $criterio)->execute();
      unset($datos);
      // Elimino las relaciones q lo tienen como 'hijo'
      $criterio->condition = 'itmCodigo=:itmCodigo';
      $criterio->params = array(':itmCodigo' => $model->itmCodigo);
      $sSQL = Yii::app()->db->commandBuilder->createDeleteCommand($this->_relationsTable, $criterio)->execute();
      // Elimino su relación con los articulos q lo tienen como clasificador
      //$criterio->condition = "CONCAT(conrutaseccion,',') LIKE '%,:secCodigo,%'";
      //$criterio->params = array(':secCodigo' => $model->secCodigo);
      //$sSQL = Yii::app()->db->commandBuilder->createDeleteCommand('tbl_conseccion', $criterio)->execute();
      // Elimino las entradas de la lista
      $criterio->condition = 'itmCodigo=:itmCodigo';
      $criterio->params = array(':itmCodigo' => $model->itmCodigo);
      $sSQL = Yii::app()->db->commandBuilder->createDeleteCommand($this->_mainTable, $criterio)->execute();

      if ($this->module->afterDelete !== '')
        eval($this->module->afterDelete . "($model->itmCodigo);");
      
      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if (!isset($_GET['ajax']))
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }
    else
      throw new CHttpException(400, Yii::t('err_CCONTROLLER_400', 'Solicitud Incorrecta. Por favor no la repita.'));
  }

  /**
   * Lists all models.
   */
  public function actionIndex() {
    $model = new Item('search');
    $model->unsetAttributes();  // clear any default values
    if (isset($_GET["Item"]))
      $model->attributes = $_GET["Item"];

    $this->render('index', array(
        'model' => $model,
    ));
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   */
  public function loadModel() {
    if ($this->_model === null) {
      if (isset($_GET['id']))
        if (is_array($_GET['id'])) {
          $id_real = $_GET['id']['itmCodigo'];
        } else {
          $id_real = $_GET['id'];
        }
      $this->_model = Item::model()->findByAttributes(array('itmCodigo' => $id_real, 'idiCodigo' => $this->_sl));
      if ($this->_model === null)
        throw new CHttpException(404, Yii::t('err_CCONTROLLER_404', 'La página solicitada no existe.'));
    }
    return $this->_model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($models) {
    if (isset($_POST['ajax']) && $_POST['ajax'] === 'Seccion-form') {
      $result = array();
      foreach ($models as $i => $item) {
        $item->itmCodigo = $_POST['Item'][$i]['itmCodigo'];
        $item->idiCodigo = $i;
        $item->itmDescripcion = $_POST['Item'][$i]['itmDescripcion'];
        $item->validate();
        foreach ($item->getErrors() as $attribute => $errors)
          $result[CHtml::activeId($item, $attribute)] = $errors;
      }
      /*
        if (!is_array($models))
        $models = array($models);
        foreach ($models as $model) {
        if ($loadInput && isset($_POST[get_class($model)]))
        $model->attributes = $_POST[get_class($model)];
        $model->validate($attributes);
        foreach ($model->getErrors() as $attribute => $errors)
        $result[CHtml::activeId($model, $attribute)] = $errors;
        }
       */
      //echo CActiveForm::validate($model);
      echo function_exists('json_encode') ? json_encode($result) : CJSON::encode($result);
      Yii::app()->end();
    }
  }

  public function actionAcceso() {
    $sSQL = <<<EOT
SELECT 
  `$this->_mainTable`.itmCodigo, 
  `$this->_mainTable`.itmDescripcion,
  `$this->_mainTable`.itmOrden 
FROM `$this->_mainTable`
WHERE `$this->_mainTable`.idiCodigo = $this->_sl AND `$this->_mainTable`.itmCodigo != 0
ORDER BY `$this->_mainTable`.itmDescripcion;
EOT;
    // retrieve items to be updated in a batch mode
    // assuming each item is of model class 'Item'
    $secciones = Seccion::model()->findAllBySql($sSQL);
    $seccionRelacion = new SeccionRelacion();
    $relaciones = SeccionRelacion::model()->findAll();
    if (isset($_POST['roles'])) {
      $aRoles = array();
      $aRol = explode("$", $_POST['roles']);
      $aPrincipales = array();
      $sSQL = "DELETE FROM $this->_relationsTable;";
      Yii::app()->db->createCommand($sSQL)->execute();

      for ($i = 0; $i < count($aRelacion); $i++) {
        $aItem = explode(",", $aRelacion[$i]);
        $sSQL = "INSERT INTO $this->_relationsTable (itmAncestro, itmCodigo) VALUES ({$aItem[0]},{$aItem[1]});";
        Yii::app()->db->createCommand($sSQL)->execute();
        $aRelaciones[] = $aItem;
      }

      for ($i = 0; $i < count($aJerarquia); $i++) {
        $aItem = explode(",", $aJerarquia[$i]);
        $sSQL = "UPDATE $this->_mainTable SET itmOrden = ({$i}+1) WHERE itmCodigo = {$aItem[0]};";
        Yii::app()->db->createCommand($sSQL)->execute();
      }

      //Una vez guardadas las novedades,
      //Buscar las rutas posibles 
      $aRutasPosibles = $this->ArmarRutas($aRelaciones);
      //Listar las de los artículos
      $sSQL = <<<EOT
SELECT DISTINCT
  `tbl_conseccion`.conrutaseccion 
FROM `tbl_conseccion`;
EOT;
      $aRutasGuardadas = Yii::app()->db->createCommand($sSQL)->queryAll();
      //DebugBreak();
      foreach ($aRutasGuardadas as $r => $ruta) {
        //buscamos si la ruta o subruta del artículo aún está en las novedades
        $bExiste = false;
        foreach ($aRutasPosibles as $p => $rutaposible) {
          //La ruta del artículo puede ser total
          //o será parcial?
          if ($rutaposible == $ruta['conrutaseccion'] ||
                  strlen(strstr($rutaposible, $ruta['conrutaseccion'] . ",")) > 0) {
            $bExiste = true;
            break; //está, sigamos...
          }
        }
        if (!$bExiste) {
          //borramos esa clasificación de los artículos
          $sSQL = "DELETE FROM tbl_conseccion WHERE `tbl_conseccion`.conrutaseccion = '{$ruta['conrutaseccion']}';";
          Yii::app()->db->createCommand($sSQL)->execute();
        }
      }
      $this->redirect(array('admin'));
    }
    $this->render('acceso', array('Secciones' => $secciones, 'relaciones' => $relaciones));
  }

  public function actionConnect() {
    $sSQL = <<<EOT
SELECT 
  `$this->_mainTable`.itmCodigo, 
  `$this->_mainTable`.itmDescripcion,
  `$this->_mainTable`.itmOrden 
FROM `$this->_mainTable`
WHERE `$this->_mainTable`.idiCodigo = '$this->_sl' AND `$this->_mainTable`.itmCodigo != 0
ORDER BY `$this->_mainTable`.itmDescripcion;
EOT;
    // retrieve items to be updated in a batch mode
    // assuming each item is of model class 'Item'
    $model = new Item;
    $items = $model->findAllBySql($sSQL);
    //$itemsRelacion = new SeccionRelacion();
    $modelR = new ItemRelacion;
    $relaciones = $modelR->findAll();
    if (isset($_POST['jerarquia']) && isset($_POST['relacion'])) {
      $aRelaciones = array();
      $aRelacion = json_decode($_POST['relacion']);
      $aJerarquia = json_decode($_POST['jerarquia']);
      $aPrincipales = array();
      $sSQL = "DELETE FROM $this->_relationsTable;";
      Yii::app()->db->createCommand($sSQL)->execute();

      foreach ($aRelacion as $i=>$aItem) {
        $sSQL = "INSERT INTO $this->_relationsTable (itmAncestro, itmCodigo) VALUES ({$aItem[0]},{$aItem[1]});";
        Yii::app()->db->createCommand($sSQL)->execute();
        $aRelaciones[] = $aItem;
      }

      foreach ($aJerarquia as $i => $aItem) {
        $sSQL = "UPDATE $this->_mainTable SET itmOrden = ({$i}+1) WHERE itmCodigo = {$aItem[0]};";
        Yii::app()->db->createCommand($sSQL)->execute();
      }

      if ($this->module->afterConnect !== '')
        eval($this->module->afterConnect.'();');

      $this->redirect(array('index'));
    }
    $this->render('connect', array('items' => $items,
        'relaciones' => $relaciones,
        'model' => $model));
  }

  //antes era ArmarRutas
  public function ListarRutas($aRelaciones = array(), $Semilla = "0", $sRutaEnCurso = "") {
    $aRutas = array();
    //busco los descendientes de $Semilla
    if ($sRutaEnCurso == "") {
      //sondeo una rama desde la raiz
      $sRutaEnCurso = $Semilla;
    } else {
      $sRutaEnCurso .= "," . $Semilla;
    }
    //Teniendo la raíz busco si tiene descendencia
    $aDescendientes = self::BuscarDescendencia($Semilla, $aRelaciones);
    if (count($aDescendientes) > 0) {
      //Si tiene descendencia debo procesar cada rama
      foreach ($aDescendientes as $d => $descendiente) {
        $xRuta = self::ListarRutas($aRelaciones, $descendiente, $sRutaEnCurso);
        //Puede venir un array o una cadena
        if (!is_array($xRuta)) {
          //devuelve una ruta completa
          $aRutas[] = $xRuta;
        } else {
          //devuelve las hojas de la rama
          $aRutas = array_merge($aRutas, $xRuta);
        }
      }
    } else {
      //Si no tiene descendencia, llegué al fondo y devuelvo la "ruta"
      return $sRutaEnCurso;
    }
    return $aRutas;
  }

  public function ListarItems($lng = null) {
    if ($lng === null)
      $lng = $this->_sl;
    return Item::model()->findAllByAttributes(array('idiCodigo' => $lng));
  }

  protected function BuscarDescendencia($Semilla = "0", $aRelaciones) {
    //busco los descendientes de $Semilla
    $aDescendientes = array();
    foreach ($aRelaciones as $p => $pares) {
      if ($Semilla == $pares[0]) {
        $aDescendientes[] = $pares[1];
      }
    }
    return $aDescendientes;
  }

  //antes era SeccionRutas
  public function ListarRelaciones() {
    $criterio = new CDBCriteria();
    $criterio->distinct = true;
    $criterio->select = array("itmAncestro", "itmCodigo");
    $criterio->order = "itmOrden";
    $gRelaciones = ItemRelacion::model()->findAll($criterio);

    foreach ($gRelaciones as $r => $relacion) {
      $aRelaciones[] = array($relacion['itmAncestro'], $relacion['itmCodigo']);
    }
    unset($gRelaciones);
    $aRutas = self::ListarRutas($aRelaciones);

    return $aRutas;
  }

  public function actionDisponiblesAjax() {
    //Se usa en el form paa elegir rutas disponibles al crear contenidos
    $sIdioma = $_GET['idIdioma'];
    $aResp = SeccionRelacion::model()->getRutasDisponibles($sIdioma);
    header("Content-type: application/json");
    echo CJSON::encode($aResp);
  }

  public function getMenuArray($seed = 0, $path = '0') {
    //No encontré manera de pasarle a Item.php las tablas 
    //o instanciarlo para que las tome de la configuración de este controlador
    $subItems = array();
    $relations = new ItemRelacion(null, $this);
    $leafs = $relations->findAllByAttributes(array('itmAncestro' => $seed));
    if (count($leafs) > 0) {
      foreach ($leafs as $leaf) {
        $tmp = self::getMenuArray($leaf['itmCodigo'], $path . ',' . $leaf['itmCodigo']);
        if ($tmp !== null)
          $subItems[] = $tmp;
      }
    }
    if ($seed != 0) {
      //Sólo procesaremos los descendientes  
      $lang = Yii::app()->language;
      $sourceLang = $this->gemiInstalled ? Yii::app()->sourceLanguage : $lang;
      $sSQL = <<<EOT
SELECT t1.itmCodigo as itmCodigo, 
       IFNULL((SELECT t2.itmDescripcion
    		 FROM $this->mainTable t2
		     WHERE (t2.itmCodigo=t1.itmCodigo AND
		            t2.idiCodigo='$lang')), t1.itmDescripcion) as itmDescripcion
FROM $this->mainTable t1 
WHERE t1.idiCodigo = '$sourceLang' and t1.itmCodigo = $seed 
EOT;
      if (eval("return !$this->showAll;"))
        $sSQL .= "and t1.itmActivo=1";
      $Items = Yii::app()->db->createCommand($sSQL)->queryAll();

      if (count($Items) > 0) {
        //Si tiene elementos, es xq está activo
        $label = $Items[0]['itmDescripcion'];
        $menu = array(
            'label' => $label,
            'url' => $this->route !== '#' ?
                    array($this->route, 'ruta' => $path) :
                    $this->route,
        );
        if ($subItems != array())
          $menu = array_merge($menu, array('items' => $subItems));
        return $menu;
      } else {
        //no tiene elementos, no está activo
        return null;
      }
    } else {
      //salimos de la iteracion y la recursividad
      // subItems tiene la estructura armada
      return $subItems;
    }
  }

  public function getGemiInstalled() {
    return $this->_gemiInstalled;
  }

  public function getMainTable() {
    return $this->_mainTable;
  }

  public function getSl() {
    return $this->_sl;
  }

  public function getSlDescripcion() {
    return $this->_slDescripcion;
  }

  public function getCodeMsg() {
    return $this->_codeMsg;
  }

  public function getTitleSingular() {
    return $this->_titleSingular;
  }

  public function getTitlePlural() {
    return $this->_titlePlural;
  }

  public function getTitleIndex() {
    return $this->_titleIndex;
  }

  public function getTitleCreate() {
    return $this->_titleCreate;
  }

  public function getTitleUpdate() {
    return $this->_titleUpdate;
  }

  public function getTitleDelete() {
    return $this->_titleDelete;
  }

  public function getTitleView() {
    return $this->_titleView;
  }

  public function getTitleHierEdit() {
    return $this->_titleHierEdit;
  }

  public function proximoCodigo() {
    return Yii::app()->db->createCommand()
                    ->select('MAX(itmCodigo) + 1 as UltimoCodigo')
                    ->from($this->_mainTable)
                    ->queryScalar();
  }

  public function TraducirRuta($sRuta = '') {
    $aRamas = explode(',', $sRuta);
    $sCamino = '/';
    $lng = Yii::app()->language;
    $sl = Yii::app()->sourceLanguage;
    unset($aRamas[0]);
    foreach ($aRamas as $v => $k) {
      $aSeccion = Item::model()->findByAttributes(array('itmCodigo' => $k, 'idiCodigo' => $lng));
      if (count($aSeccion) == 0 || empty($aSeccion->secDescripcion)) {
        $aSeccion = Item::model()->findByAttributes(array('itmCodigo' => $k, 'idiCodigo' => $sl));
      }
      $sCamino .= $aSeccion->itmDescripcion . '/';
    }
    return $sCamino;
  }

  public function ListarRutasPosibles($iSemilla = 0, $sRuta = '0', $sCamino = "/", $lng = null) {
    $subItems = array();
    $descendientes = ItemRelacion::model()->findAllByAttributes(array('itmAncestro' => $iSemilla));
    $sIdioma = $lng === null ? Yii::app()->language : $lng;
    $sl = $this->sl;
    $sSQL = <<<EOT
SELECT t1.itmCodigo as itmCodigo, 
       IFNULL((SELECT t2.itmDescripcion
    		 FROM $this->_mainTable t2
		     WHERE (t2.itmCodigo=t1.itmCodigo AND
		            t2.idiCodigo='$sIdioma')), t1.itmDescripcion) as itmDescripcion
FROM $this->_mainTable t1 
WHERE t1.idiCodigo = '$sl' and t1.itmCodigo = $iSemilla
EOT;
    $aSeccion = Yii::app()->db->createCommand($sSQL)->queryAll();
    if ($iSemilla != 0)
      $sCamino .= $aSeccion[0]['itmDescripcion'] . '/';
    if (count($descendientes) > 0) {
      foreach ($descendientes as $descendiente) {
        $subItems = array_merge($subItems, self::ListarRutasPosibles($descendiente->itmCodigo, $sRuta . ',' . $descendiente->itmCodigo, $sCamino, $sIdioma));
      }
    }
    if ($iSemilla != 0) {
      //Sólo procesaremos los descendientes  
      $menu[$sRuta] = $sCamino;
      if ($subItems != array())
        $menu = array_merge($menu, $subItems);
      return $menu;
    } else {
      //salimos de la iteracion y la recursividad
      // subItems tiene la estructura armada
      return $subItems;
    }
  }

  public function ItemsDefinidos($lng = null) {
    if ($lng === null)
      $lng = Yii::app()->sourceLanguage;
    return Yii::app()->db->createCommand("SELECT COUNT(*) from $this->mainTable WHERE idiCodigo = '$lng' and itmCodigo <> 0")->queryScalar();
  }

}